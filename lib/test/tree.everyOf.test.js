import { describe, expect, it } from "vitest";
import {
  invalidPathElements,
  isAnimal,
  isElectronic,
  isMineral,
  isNotMineral,
  isNotToggledZero,
  isToggledZero,
  isVegetable,
  prepareDistinctTree,
} from './tree.utils';

// everyOf(callbackfn: (datum:Datum, tree: Tree<FullPath, Datum>) => boolean, path?: Path, depth?: number): boolean;

describe(`The Tree's everyOf(callbackfn, path?, depth? ) method`, () => {
  let t;
  beforeEach(() => {
    t = prepareDistinctTree();
  });

  it(`should throw (like Map.forEach()) when callbackfn is not provided, is undefined, or is not a function`, () => {
    expect(() => t.everyOf()).toThrow();
    const cb = undefined;
    expect(() => t.everyOf(cb)).toThrow;
    expect(() => t.everyOf('foo')).toThrow;
  });

  it(`should return false when path is invalid`, () => {
    let v;
    invalidPathElements.forEach((v) => {
      // path elements
      expect(t.everyOf(isVegetable, v)).toBeFalsy();

      // full paths
      expect(t.everyOf(isVegetable, [v])).toBeFalsy();
      expect(t.everyOf(isVegetable, [0, v])).toBeFalsy();
      expect(t.everyOf(isVegetable, [0, 9, v])).toBeFalsy();
    });
  });

  describe(`when provided a path as a PathElement argument`, () => {

    it(`should return false when the PathElement does not exist in the Tree`, () => {
      expect(t.everyOf(isVegetable, 97)).toBeFalsy();
      expect(t.everyOf(isVegetable, 98, 1)).toBeFalsy();
    });

    describe(`when not provided a depth argument`, () => {

      it(`should return false when any Datum for the path and its descendents does not pass the test implemented by the provided callback`, () => {
        expect(t.everyOf(isVegetable, 0)).toBeFalsy();
      });

      it(`should return true when all Entries for the path and its descendents pass the test implemented by the provided callback`, () => {
        expect(t.everyOf(isAnimal, 1)).toBeTruthy();
      });

    });

    describe(`when provided a depth argument`, () => {
      it(`should return false when the depth is not an integer`, () => {
        expect(t.everyOf(isAnimal, 1, false)).toBeFalsy();
      });

      it(`should return false when the depth is a negative integer`, () => {
        expect(t.everyOf(isAnimal, 1, -1)).toBeFalsy();
      });

      it(`should return false when the depth is greater than 9007199254740991 (Number.MAX_SAFE_INTEGER)`, () => {
        const n = 9007199254740991 + 1;
        expect(t.everyOf(isAnimal, 1, n)).toBeFalsy();
      });

      it(`should return false when a depth of zero is provided and the Datum for the path does not pass the test implemented by the provided callback`, () => {
        expect(t.everyOf(isMineral, 0, 0)).toBeFalsy();
        expect(t.everyOf(isMineral, 1, 0)).toBeFalsy();
        expect(t.everyOf(isMineral, 4, 0)).toBeFalsy();
      });

      it(`should return true when a depth of zero is provided and the Datum for the path passes the test implemented by the provided callback`, () => {
        expect(t.everyOf(isVegetable, 0, 0)).toBeTruthy();
        expect(t.everyOf(isVegetable, 5, 0)).toBeTruthy();
        expect(t.everyOf(isVegetable, 8, 0)).toBeTruthy();
      });

      it(`should return false when a depth greater than zero is provided and the Entries for the path and its descendents do not pass the test implemented by the provided callback`, () => {
        // multiple depths apply to test here
        expect(t.everyOf(isMineral, 0, 99)).toBeFalsy();
        expect(t.everyOf(isMineral, 0, 3)).toBeFalsy();
        expect(t.everyOf(isMineral, 1, 2)).toBeFalsy();
        expect(t.everyOf(isMineral, 4, 1)).toBeFalsy();
        expect(t.everyOf(isMineral, 4, 99)).toBeFalsy();
      });

      it(`should return true when a depth greater than zero is provided and the Entries for the path and its descendents pass the test implemented by the provided callback`, () => {
        // multiple depths apply to test here
        expect(t.everyOf(isVegetable, 5, 99)).toBeTruthy();
        expect(t.everyOf(isVegetable, 5, 2)).toBeTruthy();
        expect(t.everyOf(isVegetable, 8, 1)).toBeTruthy();
        expect(t.everyOf(isVegetable, 8, 99)).toBeTruthy();
      });
    });
  });

  describe(`when provided a path as a FullPath argument`, () => {

    it(`should return false when the FullPath does not exist in the Tree`, () => {
      expect(t.everyOf(isVegetable, [0, 99])).toBeFalsy();
      expect(t.everyOf(isVegetable, [0, 99], 1)).toBeFalsy();
    });

    describe(`when not provided a depth argument`, () => {

      it(`should return false when any Datum for the path and its descendents does not pass the test implemented by the provided callback`, () => {
        expect(t.everyOf(isVegetable, [0])).toBeFalsy();
      });

      it(`should return true when all Entries for the path and its descendents pass the test implemented by the provided callback`, () => {
        expect(t.everyOf(isAnimal, [0, 1])).toBeTruthy();
      })

    });

    describe(`when provided a depth argument`, () => {
      it(`should return false when the depth is not an integer`, () => {
        expect(t.everyOf(isAnimal, [0, 1], false)).toBeFalsy();
      });

      it(`should return false when the depth is a negative integer`, () => {
        expect(t.everyOf(isAnimal, [0, 1], -1)).toBeFalsy();
      });

      it(`should return false when the depth is greater than 9007199254740991 (Number.MAX_SAFE_INTEGER)`, () => {
        const n = 9007199254740991 + 1;
        expect(t.everyOf(isAnimal, [0, 1], n)).toBeFalsy();
      });

      it(`should return false when a depth of zero is provided and the Datum for the path does not pass the test implemented by the provided callback`, () => {
        expect(t.everyOf(isMineral, [0], 0)).toBeFalsy();
        expect(t.everyOf(isMineral, [0, 1], 0)).toBeFalsy();
        expect(t.everyOf(isMineral, [0, 1, 4], 0)).toBeFalsy();
      });

      it(`should return true when a depth of zero is provided and the Datum for the path passes the test implemented by the provided callback`, () => {
        expect(t.everyOf(isVegetable, [0], 0)).toBeTruthy();
        expect(t.everyOf(isVegetable, [0, 5], 0)).toBeTruthy();
        expect(t.everyOf(isVegetable, [0, 5, 8], 0)).toBeTruthy();
      });

      it(`should return false when any Datum for the path and its descendents does not pass the test implemented by the provided callback`, () => {
        expect(t.everyOf(isAnimal, [0], 99)).toBeFalsy();
        expect(t.everyOf(isAnimal, [0], 3)).toBeFalsy();
        expect(t.everyOf(isAnimal, [0, 5], 2)).toBeFalsy();
        expect(t.everyOf(isAnimal, [0, 5, 8], 1)).toBeFalsy();
        expect(t.everyOf(isAnimal, [0, 5, 8], 99)).toBeFalsy();
      });

      it(`should return true when all Entries for the path and its descendents pass the test implemented by the provided callback`, () => {
        // NOT mineral
        expect(t.everyOf(isNotMineral, [0, 1], 99)).toBeTruthy();
        expect(t.everyOf(isNotMineral, [0, 1], 3)).toBeTruthy();
        expect(t.everyOf(isNotMineral, [0, 5], 2)).toBeTruthy();
        expect(t.everyOf(isNotMineral, [0, 5, 8], 1)).toBeTruthy();
        expect(t.everyOf(isNotMineral, [0, 5, 8], 99)).toBeTruthy();
      });
    });
  });

  describe(`when not provided a path or path is undefined`, () => {
    describe(`when provided a depth argument`, () => {

      it(`should return false when the depth is not an integer`, () => {
        expect(t.everyOf(isAnimal, undefined, false)).toBeFalsy();
      });

      it(`should return false when the depth is a negative integer`, () => {
        expect(t.everyOf(isAnimal, undefined, -1)).toBeFalsy();
      });

      it(`should return false when the depth is greater than 9007199254740991 (Number.MAX_SAFE_INTEGER)`, () => {
        const n = 9007199254740991 + 1;
        expect(t.everyOf(isAnimal, undefined, n)).toBeFalsy();
      });

      it(`should return false when a depth of zero is provided and the Entries with a depth of zero do not pass the test implemented by the provided callback`, () => {
        expect(t.everyOf(isAnimal, undefined, 0)).toBeFalsy();
        expect(t.everyOf(isMineral, undefined, 0)).toBeFalsy();
        expect(t.everyOf(isVegetable, undefined, 0)).toBeFalsy();
        expect(t.everyOf(isNotToggledZero, undefined, 0)).toBeFalsy();
      });

      it(`should return true when a depth of zero is provided and the Entries with a depth of zero passes the test implemented by the provided callback`, () => {
        expect(t.everyOf(isToggledZero, undefined, 0)).toBeTruthy();
      });

      it(`should return false when a depth greater than zero is provided and the Entries for the path and its descendents do not pass the test implemented by the provided callback`, () => {
        // multiple depths apply to test here
        expect(t.everyOf(isNotToggledZero, undefined, 0)).toBeFalsy(); // also tested above
        expect(t.everyOf(isNotToggledZero, undefined, 1)).toBeFalsy();
        expect(t.everyOf(isNotToggledZero, undefined, 2)).toBeFalsy();
        expect(t.everyOf(isNotToggledZero, undefined, 3)).toBeFalsy();
        expect(t.everyOf(isNotToggledZero, undefined, 99)).toBeFalsy();
        expect(t.everyOf(isAnimal, undefined, 99)).toBeFalsy();
        expect(t.everyOf(isMineral, undefined, 99)).toBeFalsy();
        expect(t.everyOf(isVegetable, undefined, 99)).toBeFalsy();

      });

      it(`should return true when a depth greater than zero is provided and the Entries for the path and its descendents pass the test implemented by the provided callback`, () => {
        // multiple depths apply to test here
        expect(t.everyOf(isToggledZero, undefined, 0)).toBeTruthy(); // also tested above
        expect(t.everyOf(isToggledZero, undefined, 1)).toBeTruthy();
        expect(t.everyOf(isToggledZero, undefined, 2)).toBeTruthy();
        expect(t.everyOf(isToggledZero, undefined, 3)).toBeTruthy();
        expect(t.everyOf(isToggledZero, undefined, 99)).toBeTruthy();
      });
    });
  });

    describe(`when not provided a depth argument`, () => {
      it(`should return false when all Entries do not pass the test implemented by the provided callback`, () => {
        expect(t.everyOf(isAnimal, undefined)).toBeFalsy();
        expect(t.everyOf(isMineral)).toBeFalsy();
        expect(t.everyOf(isNotToggledZero)).toBeFalsy();
      });
      it(`should return true when all Entries pass the test implemented by the provided callback`, () => {
        expect(t.everyOf(isToggledZero)).toBeTruthy();
      });

    });

});
