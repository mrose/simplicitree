import { describe, expect, it } from "vitest";
import { prepareDistinctTree } from "./tree.utils";

// traverse(callbackfn: (datum: Datum, tree: Tree<FullPath, Datum>) => void, path: Path, order?: Order): boolean;

describe(`The Tree's traverse(callbackfn, path, order ) method`, () => {
  let cb, t;
  beforeEach(() => {
    t = prepareDistinctTree();
  });

  it(`should throw (like Map.forEach()) when callbackfn is not provided, is undefined, or is not a function`, () => {
    cb = undefined;
    expect(() => t.traverse()).toThrow();
    expect(() => t.traverse(cb)).toThrow();
    expect(() => t.traverse(cb, 1)).toThrow();
    expect(() => t.traverse(cb, 1, 'asc')).toThrow();
    expect(() => t.traverse(cb, [0, 1], 'asc')).toThrow();
    expect(() => t.traverse('foo')).toThrow();
  });


  it(`should return false when path is not provided or undefined`, () => {
    cb = (datum, tree) => 'foo';
    expect(t.traverse(cb)).toBeFalsy();
    expect(t.traverse(cb, undefined)).toBeFalsy();
    const u = undefined;
    expect(t.traverse(cb, u)).toBeFalsy();
  });

  it(`should return false when path is invalid`, () => {
    cb = (datum, tree) => 'foo';
    const a = [];
    const f = function () { return 'foo' };
    const m = new Map();
    const n = null;
    const re = new RegExp("ab+c", "i");
    const s = new Set();
    const u = undefined;

    // path elements
    expect(t.traverse(cb, a)).toBeFalsy();
    expect(t.traverse(cb, f)).toBeFalsy();
    expect(t.traverse(cb, m)).toBeFalsy();
    expect(t.traverse(cb, n)).toBeFalsy();
    expect(t.traverse(cb, s)).toBeFalsy();
    expect(t.traverse(cb, u)).toBeFalsy();

    // full paths
    expect(t.traverse(cb, [a])).toBeFalsy();
    expect(t.traverse(cb, [f])).toBeFalsy();
    expect(t.traverse(cb, [m])).toBeFalsy();
    expect(t.traverse(cb, [n])).toBeFalsy();
    expect(t.traverse(cb, [s])).toBeFalsy();
    expect(t.traverse(cb, [u])).toBeFalsy();

    expect(t.traverse(cb, [0, a])).toBeFalsy();
    expect(t.traverse(cb, [0, f])).toBeFalsy();
    expect(t.traverse(cb, [0, m])).toBeFalsy();
    expect(t.traverse(cb, [0, n])).toBeFalsy();
    expect(t.traverse(cb, [0, s])).toBeFalsy();
    expect(t.traverse(cb, [0, u])).toBeFalsy();

    expect(t.traverse(cb, [0, 9, n])).toBeFalsy();
  });

  it(`it should return false when the order is provided and is not 'asc' or desc'`, () => {
    cb = (datum, tree) => 'foo';
    expect(t.traverse(cb, 1, 'noodles')).toBeFalsy();
    expect(t.traverse(cb, [0, 1], 'poodles')).toBeFalsy();
  });

  it(`should propogate the exception when the callback throws`, () => {
    cb = (datum, tree) => { throw new Error("intentional traverse callback error") };
    expect(() => t.traverse(cb, 0)).toThrow();
    expect(() => t.traverse(cb, [0, 1])).toThrow();
    expect(() => t.traverse(cb, 0, 'asc')).toThrow();
    expect(() => t.traverse(cb, [0, 1], 'asc')).toThrow();
  });

  describe(`when provided a path as a FullPath argument`, () => {

    it(`should return false when the path does not exist in the Tree`, () => {
      const cb = (datum, tree) => 'foo';
      expect(t.traverse(cb, [0, 97])).toBeFalsy();
      expect(t.traverse(cb, [0, 98], 'asc')).toBeFalsy();
      expect(t.traverse(cb, [0, 2, 4, 16], 'asc')).toBeFalsy();
    });

    describe(`when not provided an order argument`, () => {
      it(`should apply the callback to each Datum within the path,
      starting from the path's root, and return true`, () => {
        let path = [0, 9, 12, 13];
        let count = 0;
        cb = (datum, tree) => {
          let { id, name, type, toggle } = datum;
          datum.toggle = ++count;
        };
        expect(t.traverse(cb, path)).toBeTruthy();
        const e0 = t.get([0]);
        const e9 = t.get([0, 9]);
        const e12 = t.get([0, 9, 12]);
        const e13 = t.get([0, 9, 12, 13]);
        expect(e0.toggle).toEqual(1);
        expect(e9.toggle).toEqual(2);
        expect(e12.toggle).toEqual(3);
        expect(e13.toggle).toEqual(4);
      });

    });
    describe(`when provided an order argument`, () => {
      describe(`when the order is 'desc' (descending`, () => {
        it(`should apply the callback to each Datum within the path,
        starting from the node's root and ending at the node, and return true`, () => {
          let path = [0, 9, 12, 13];
          let count = 0;
          cb = (datum, tree) => {
            let { id, name, type, toggle } = datum;
            datum.toggle = ++count;
          };
          expect(t.traverse(cb, path, 'desc')).toBeTruthy();
          const e0 = t.get([0]);
          const e9 = t.get([0, 9]);
          const e12 = t.get([0, 9, 12]);
          const e13 = t.get([0, 9, 12, 13]);
          expect(e0.toggle).toEqual(1);
          expect(e9.toggle).toEqual(2);
          expect(e12.toggle).toEqual(3);
          expect(e13.toggle).toEqual(4);

        });

      });
      describe(`when the order is 'asc' (ascending`, () => {
        it(`should apply the callback to each Datum within the path,
        starting from the node and ending at the root, and return true`, () => {
          let path = [0, 9, 12, 13];
          let count = 0;
          cb = (datum, tree) => {
            let { id, name, type, toggle } = datum;
            datum.toggle = ++count;
          };
          expect(t.traverse(cb, path, 'asc')).toBeTruthy();
          const e0 = t.get([0]);
          const e9 = t.get([0, 9]);
          const e12 = t.get([0, 9, 12]);
          const e13 = t.get([0, 9, 12, 13]);
          expect(e13.toggle).toEqual(1);
          expect(e12.toggle).toEqual(2);
          expect(e9.toggle).toEqual(3);
          expect(e0.toggle).toEqual(4);
        });
      });
    });
  });

  describe(`when provided a path as a PathElement argument`, () => {
    it(`should return false when the path does not exist in the Tree`, () => {
      const cb = (datum, tree) => 'foo';
      expect(t.traverse(cb, 97)).toBeFalsy();
      expect(t.traverse(cb, 98, 'asc')).toBeFalsy();
      expect(t.traverse(cb, 99, 'desc')).toBeFalsy();
    });

    describe(`when not provided an order argument`, () => {
      it(`should apply the callback to each Datum within the path,
      starting from the path's root, and return true`, () => {
        let path = 13;
        let count = 0;
        cb = (datum, tree) => {
          let { id, name, type, toggle } = datum;
          datum.toggle = ++count;
        };
        expect(t.traverse(cb, path)).toBeTruthy();
        const e0 = t.getNode(0);
        const e9 = t.getNode(9);
        const e12 = t.getNode(12);
        const e13 = t.getNode(13);
        expect(e0.toggle).toEqual(1);
        expect(e9.toggle).toEqual(2);
        expect(e12.toggle).toEqual(3);
        expect(e13.toggle).toEqual(4);
      });

    });
    describe(`when provided an order argument`, () => {
      describe(`when the order is 'desc' (descending`, () => {
        it(`should apply the callback to each Datum within the path,
        starting from the node's root and ending at the node, and return true`, () => {
          let path = 13;
          let count = 0;
          cb = (datum, tree) => {
            let { id, name, type, toggle } = datum;
            datum.toggle = ++count;
          };
          expect(t.traverse(cb, path, 'desc')).toBeTruthy();
          const e0 = t.getNode(0);
          const e9 = t.getNode(9);
          const e12 = t.getNode(12);
          const e13 = t.getNode(13);
          expect(e0.toggle).toEqual(1);
          expect(e9.toggle).toEqual(2);
          expect(e12.toggle).toEqual(3);
          expect(e13.toggle).toEqual(4);
        });

      });
      describe(`when the order is 'asc' (ascending`, () => {
        it(`should apply the callback to each Datum within the path,
        starting from the node and ending at the root, and return true`, () => {
          let path = 13;
          let count = 0;
          cb = (datum, tree) => {
            let { id, name, type, toggle } = datum;
            datum.toggle = ++count;
          };
          expect(t.traverse(cb, path, 'asc')).toBeTruthy();
          const e0 = t.getNode(0);
          const e9 = t.getNode(9);
          const e12 = t.getNode(12);
          const e13 = t.getNode([0, 9, 12, 13]); // full path will work with getNode
          expect(e13.toggle).toEqual(1);
          expect(e12.toggle).toEqual(2);
          expect(e9.toggle).toEqual(3);
          expect(e0.toggle).toEqual(4);
        });
      });
    });







  });
});
