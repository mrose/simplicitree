import { describe, expect, it } from "vitest";
import {
  invalidPathElements,
  isAnimal,
  isElectronic,
  isMineral,
  isNotMineral,
  isNotToggledZero,
  isToggledZero,
  isVegetable,
  prepareDistinctTree,
} from './tree.utils';

// someOf(callbackfn: (datum:Datum, tree: Tree<FullPath, Datum>) => boolean, path?: Path, depth?: number): boolean;

describe(`The Tree's someOf(callbackfn, path?, depth? ) method`, () => {
  let t;
  beforeEach(() => {
    t = prepareDistinctTree();
  });

  it(`should throw (like Map.forEach()) when callbackfn is not provided, is undefined, or is not a function`, () => {
    expect(() => t.someOf()).toThrow();
    const cb = undefined;
    expect(() => t.someOf(cb)).toThrow;
    expect(() => t.someOf('foo')).toThrow;
  });

  it(`should return false when path is invalid`, () => {
    let v;
    invalidPathElements.forEach((v) => {
      // path elements
      expect(t.someOf(isVegetable, v)).toBeFalsy();

      // full paths
      expect(t.someOf(isVegetable, [v])).toBeFalsy();
      expect(t.someOf(isVegetable, [0, v])).toBeFalsy();
      expect(t.someOf(isVegetable, [0, 9, v])).toBeFalsy();
    });
  });

  describe(`when provided a path as a PathElement argument`, () => {

    it(`should return false when the PathElement does not exist in the Tree`, () => {
      expect(t.someOf(isVegetable, 97)).toBeFalsy();
      expect(t.someOf(isVegetable, 98, 1)).toBeFalsy();
    });

    describe(`when not provided a depth argument`, () => {

      it(`should return false when all Entries for the path and its descendents do not pass the test implemented by the provided callback`, () => {
        expect(t.someOf(isElectronic, 0)).toBeFalsy();
      });

      it(`should return true when at least one Datum for the path and its descendents pass the test implemented by the provided callback`, () => {
        expect(t.someOf(isAnimal, 1)).toBeTruthy();
      });

    });

    describe(`when provided a depth argument`, () => {
      it(`should return false when the depth is not an integer`, () => {
        expect(t.someOf(isAnimal, 1, false)).toBeFalsy();
      });

      it(`should return false when the depth is a negative integer`, () => {
        expect(t.someOf(isAnimal, 1, -1)).toBeFalsy();
      });

      it(`should return false when the depth is greater than 9007199254740991 (Number.MAX_SAFE_INTEGER)`, () => {
        const n = 9007199254740991 + 1;
        expect(t.someOf(isAnimal, 1, n)).toBeFalsy();
      });

      it(`should return false when a depth of zero is provided and the Datum for the path does not pass the test implemented by the provided callback`, () => {
        expect(t.someOf(isMineral, 0, 0)).toBeFalsy();
        expect(t.someOf(isMineral, 1, 0)).toBeFalsy();
        expect(t.someOf(isMineral, 4, 0)).toBeFalsy();
      });

      it(`should return true when a depth of zero is provided and the Datum for the path passes the test implemented by the provided callback`, () => {
        expect(t.someOf(isVegetable, 0, 0)).toBeTruthy();
        expect(t.someOf(isVegetable, 5, 0)).toBeTruthy();
        expect(t.someOf(isVegetable, 8, 0)).toBeTruthy();
      });

      it(`should return false when a depth greater than zero is provided and all Entries for the path and its descendents do not pass the test implemented by the provided callback`, () => {
        // multiple depths apply to test here
        expect(t.someOf(isElectronic, 0, 99)).toBeFalsy();
        expect(t.someOf(isElectronic, 0, 3)).toBeFalsy();
        expect(t.someOf(isElectronic, 1, 2)).toBeFalsy();
        expect(t.someOf(isElectronic, 4, 1)).toBeFalsy();
        expect(t.someOf(isElectronic, 4, 99)).toBeFalsy();
      });

      it(`should return true when a depth greater than zero is provided and at least one Datum for the path and its descendents passes the test implemented by the provided callback`, () => {
        // multiple depths apply to test here
        expect(t.someOf(isVegetable, 5, 99)).toBeTruthy();
        expect(t.someOf(isVegetable, 5, 2)).toBeTruthy();
        expect(t.someOf(isVegetable, 8, 1)).toBeTruthy();
        expect(t.someOf(isVegetable, 8, 99)).toBeTruthy();
      });
    });
  });

  describe(`when provided a path as a FullPath argument`, () => {

    it(`should return false when the FullPath does not exist in the Tree`, () => {
      expect(t.someOf(isVegetable, [0, 99])).toBeFalsy();
      expect(t.someOf(isVegetable, [0, 99], 1)).toBeFalsy();
    });

    describe(`when not provided a depth argument`, () => {

      it(`should return false when all Entries for the path and its descendents do not pass the test implemented by the provided callback`, () => {
        expect(t.someOf(isElectronic, [0])).toBeFalsy();
      });

      it(`should return true when at least one Datum for the path and its descendents passes the test implemented by the provided callback`, () => {
        expect(t.someOf(isAnimal, [0, 1])).toBeTruthy();
      })

    });

    describe(`when provided a depth argument`, () => {
      it(`should return false when the depth is not an integer`, () => {
        expect(t.someOf(isAnimal, [0, 1], false)).toBeFalsy();
      });

      it(`should return false when the depth is a negative integer`, () => {
        expect(t.someOf(isAnimal, [0, 1], -1)).toBeFalsy();
      });

      it(`should return false when the depth is greater than 9007199254740991 (Number.MAX_SAFE_INTEGER)`, () => {
        const n = 9007199254740991 + 1;
        expect(t.someOf(isAnimal, [0, 1], n)).toBeFalsy();
      });

      it(`should return false when a depth of zero is provided and the Datum for the path does not pass the test implemented by the provided callback`, () => {
        expect(t.someOf(isMineral, [0], 0)).toBeFalsy();
        expect(t.someOf(isMineral, [0, 1], 0)).toBeFalsy();
        expect(t.someOf(isMineral, [0, 1, 4], 0)).toBeFalsy();
      });

      it(`should return true when a depth of zero is provided and the Datum for the path passes the test implemented by the provided callback`, () => {
        expect(t.someOf(isVegetable, [0], 0)).toBeTruthy();
        expect(t.someOf(isVegetable, [0, 5], 0)).toBeTruthy();
        expect(t.someOf(isVegetable, [0, 5, 8], 0)).toBeTruthy();
      });

      it(`should return false when all Entries for the path and its descendents do not pass the test implemented by the provided callback`, () => {
        expect(t.someOf(isElectronic, [0], 99)).toBeFalsy();
        expect(t.someOf(isElectronic, [0], 3)).toBeFalsy();
        expect(t.someOf(isElectronic, [0, 5], 2)).toBeFalsy(); // all veg
        expect(t.someOf(isElectronic, [0, 5, 8], 1)).toBeFalsy(); // ditto
        expect(t.someOf(isElectronic, [0, 5, 8], 99)).toBeFalsy(); // ditto
      });

      it(`should return true when at least one Datum for the path and its descendents passes the test implemented by the provided callback`, () => {
        expect(t.someOf(isNotMineral, [0, 1], 99)).toBeTruthy();
        expect(t.someOf(isNotMineral, [0, 1], 3)).toBeTruthy();
        expect(t.someOf(isNotMineral, [0, 5], 2)).toBeTruthy();
        expect(t.someOf(isNotMineral, [0, 5, 8], 1)).toBeTruthy();
        expect(t.someOf(isNotMineral, [0, 5, 8], 99)).toBeTruthy();
      });
    });
  });

  describe(`when not provided a path or path is undefined`, () => {
    describe(`when provided a depth argument`, () => {

      it(`should return false when the depth is not an integer`, () => {
        expect(t.someOf(isAnimal, undefined, false)).toBeFalsy();
      });

      it(`should return false when the depth is a negative integer`, () => {
        expect(t.someOf(isAnimal, undefined, -1)).toBeFalsy();
      });

      it(`should return false when the depth is greater than 9007199254740991 (Number.MAX_SAFE_INTEGER)`, () => {
        const n = 9007199254740991 + 1;
        expect(t.someOf(isAnimal, undefined, n)).toBeFalsy();
      });

      it(`should return false when a depth of zero is provided and the Entries with a depth of zero do not pass the test implemented by the provided callback`, () => {
        expect(t.someOf(isAnimal, undefined, 0)).toBeFalsy();
        expect(t.someOf(isMineral, undefined, 0)).toBeFalsy();
        expect(t.someOf(isNotToggledZero, undefined, 0)).toBeFalsy();
      });

      it(`should return true when a depth of zero is provided and at least one Entry with a depth of zero passes the test implemented by the provided callback`, () => {
        expect(t.someOf(isToggledZero, undefined, 0)).toBeTruthy();
      });

      it(`should return false when a depth greater than zero is provided and no Entries for the path and its descendents pass the test implemented by the provided callback`, () => {
        // multiple depths apply to test here
        expect(t.someOf(isNotToggledZero, undefined, 0)).toBeFalsy(); // also tested above
        expect(t.someOf(isNotToggledZero, undefined, 1)).toBeFalsy();
        expect(t.someOf(isNotToggledZero, undefined, 2)).toBeFalsy();
        expect(t.someOf(isNotToggledZero, undefined, 3)).toBeFalsy();
        expect(t.someOf(isNotToggledZero, undefined, 99)).toBeFalsy();
      });

      it(`should return true when a depth greater than zero is provided and at least one Entry for the path and its descendents passes the test implemented by the provided callback`, () => {
        // multiple depths apply to test here
        expect(t.someOf(isToggledZero, undefined, 0)).toBeTruthy(); // also tested above
        expect(t.someOf(isToggledZero, undefined, 1)).toBeTruthy();
        expect(t.someOf(isToggledZero, undefined, 2)).toBeTruthy();
        expect(t.someOf(isToggledZero, undefined, 3)).toBeTruthy();
        expect(t.someOf(isToggledZero, undefined, 99)).toBeTruthy();
      });
    });
  
    describe(`when not provided a depth argument`, () => {

      it(`should return false when all Entries do not pass the test implemented by the provided callback`, () => {
       expect(t.someOf(isNotToggledZero)).toBeFalsy();
       expect(t.someOf(isElectronic)).toBeFalsy();
      });
      it(`should return true when at least one Entry passes the test implemented by the provided callback`, () => {
        expect(t.someOf(isToggledZero)).toBeTruthy();
      });

    });
  });
});
