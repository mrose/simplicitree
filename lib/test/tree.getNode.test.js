import { describe, expect, it } from "vitest";
import { Tree } from "../tree";

describe(`The Tree's getNode( path ) method,`, () => {

  it(`should return undefined when provided an invalid path element`, () => {
    const t = new Tree();
    expect(() => t.getNode(null)).not.toThrow();
    expect(() => t.getNode(undefined)).not.toThrow();
    expect(() => t.getNode(() => 'foo')).not.toThrow();
    expect(() => t.getNode(/ab+c/i)).not.toThrow();

    expect(t.getNode(null)).toBeUndefined();
    expect(t.getNode(undefined)).toBeUndefined();
    expect(t.getNode(() => 'foo')).toBeUndefined();
    expect(t.getNode(/ab+c/i)).toBeUndefined();
  });

  it(`should return the value when provided an existing path element`, () => {
    const t = new Tree();
    let actual, expected, key1, key2, key3, key4;
    key1 = ['root'];
    key2 = ['root', 'a'];
    key3 = ['root', 'a', 'b'];
    key4 = ['root', 'a', 'b', 'c'];
    t.set(key1, { id: 'root', name: 'oak' });
    t.set(key2, { id: 'a', name: 'anthony' });
    t.set(key3, { id: 'b', name: 'bettina' });
    t.set(key4, { id: 'c', name: 'caroline' });

    actual = t.getNode('root');
    expected = { id: 'root', name: 'oak' };
    expect(actual).toEqual(expected);
    actual = t.getNode('a');
    expected = { id: 'a', name: 'anthony' };
    expect(actual).toEqual(expected);
    actual = t.getNode('b');
    expected = { id: 'b', name: 'bettina' };
    expect(actual).toEqual(expected);
    actual = t.getNode('c');
    expected = { id: 'c', name: 'caroline' };
    expect(actual).toEqual(expected);
  });

  it(`should return undefined when provided a non-existing path element`, () => {
    const t = new Tree();
    let actual, key1, key2, key3, key4;
    key1 = ['root'];
    key2 = ['root', 'a'];
    key3 = ['root', 'a', 'b'];
    key4 = ['root', 'a', 'b', 'c'];
    t.set(key1, { id: 'root', name: 'oak' });
    t.set(key2, { id: 'a', name: 'anthony' });
    t.set(key3, { id: 'b', name: 'bettina' });
    t.set(key4, { id: 'c', name: 'caroline' });

    actual = t.getNode('fruit');
    expect(actual).toBeUndefined;
    actual = t.getNode('root,a,b,c');
    expect(actual).toBeUndefined;
    actual = t.getNode('');
    expect(actual).toBeUndefined;
  });

  it(`should retrieve values for keys which are arrays`, () => {
    const t = new Tree();
    let actual, expected, key1, key2;

    // NOTE: array keys are set by REFERENCE, not by VALUE

    key1 = ['root'];
    t.set(key1, { id: 'root', name: 'oak' });
    actual = t.getNode(key1);
    expected = { id: 'root', name: 'oak' };
    expect(actual).toStrictEqual(expected);

    key2 = ['root', 'a', 'b', 'c'];
    t.set(key2, { id: 'c', name: 'caroline' });
    actual = t.getNode(key2);
    expected = { id: 'c', name: 'caroline' };
    expect(actual).toStrictEqual(expected);

    expected = [
      [key1, { id: 'root', name: 'oak' }],
      [key2, { id: 'c', name: 'caroline' }]
    ];
    actual = [...t.entries()];
    expect(actual).toStrictEqual(expected);

    // note the keys and see the magic of spread
    expected = [
      [['root'], { id: 'root', name: 'oak' }],
      [['root', 'a', 'b', 'c'], { id: 'c', name: 'caroline' }]
    ];
    actual = [...t.entries()];
    expect(actual).toStrictEqual(expected);
  });

  it(`should return undefined when provided an invalid full path`, () => {
    const t = new Tree();
    const ifp = ['foo', 'bar', ''];
    expect(() => t.getNode(ifp)).not.toThrow();
    expect(() => t.getNode([])).not.toThrow();
    expect(t.getNode(ifp)).toBeUndefined();
    expect(t.getNode([])).toBeUndefined();
  });

  it(`should return the value when provided an existing full path`, () => {
    const t = new Tree();

    // NOTE: array keys are set by REFERENCE, not by VALUE

    let actual, key1, key2;
    key1 = ['root'];
    key2 = ['root', 'a', 'b', 'c'];
    t.set(key1, { id: 'root', name: 'oak' });
    t.set(key2, { id: 'c', name: 'caroline' });
    actual = t.getNode(key1);
    expect(actual).toEqual({ id: 'root', name: 'oak' });
    actual = t.getNode(key2);
    expect(actual).toEqual({ id: 'c', name: 'caroline' });
  });

  it(`should return undefined when provided a non-existing full path`, () => {
    const t = new Tree();
    let actual, key1, key2, key3, key4;
    key1 = ['root'];
    key2 = ['root', 'a'];
    key3 = ['root', 'a', 'b'];
    key4 = ['root', 'a', 'b', 'c'];
    t.set(key1, { id: 'root', name: 'oak' });
    t.set(key2, { id: 'a', name: 'anthony' });
    t.set(key3, { id: 'b', name: 'bettina' });
    t.set(key4, { id: 'c', name: 'caroline' });

    actual = t.getNode(['root', 'x', 'y', 'z']);
    expect(actual).toBeUndefined;

    actual = t.getNode(['root,x,y,z']);
    expect(actual).toBeUndefined;

    actual = t.getNode(['root,a,b,c']);
    expect(actual).toBeUndefined;

  });

});
