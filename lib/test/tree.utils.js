import { Tree } from '../tree';

const invalidPathElements = [
  [],
  function () { return 'foo' },
  new Map(),
  null,
  new RegExp('ab+c', 'i'),
  new Set(),
];
//undefined, // sometimes undefined is ok

// a 'distinct' tree is one where every id is unique
const prepareDistinctTree = () => {
  let t = new Tree();
  t.set([0], { id: 0, name: 'root', type: 'vegetable', toggle: 0 });
  t.set([0, 1], { id: 1, name: 'alsatian', type: 'animal', toggle: 0 });
  t.set([0, 1, 2], { id: 2, name: 'beagle', type: 'animal', toggle: 0 });
  t.set([0, 1, 3], { id: 3, name: 'cavador', type: 'animal', toggle: 0 });
  t.set([0, 1, 4], { id: 4, name: 'daschund', type: 'animal', toggle: 0 });
  t.set([0, 5], { id: 5, name: 'eggplant', type: 'vegetable', toggle: 0 });
  t.set([0, 5, 6], { id: 6, name: 'fiddlehead', type: 'vegetable', toggle: 0 });
  t.set([0, 5, 7], { id: 7, name: 'gnetum', type: 'vegetable', toggle: 0 });
  t.set([0, 5, 8], { id: 8, name: 'h', type: 'vegetable', toggle: 0 });
  t.set([0, 9], { id: 9, name: 'iron', type: 'mineral', toggle: 0 });
  t.set([0, 9, 10], { id: 10, name: 'jadeite', type: 'mineral', toggle: 0 });
  t.set([0, 9, 11], { id: 11, name: 'kryptonite', type: 'mineral', toggle: 0 });
  t.set([0, 9, 12], { id: 12, name: 'luddite', type: 'mineral', toggle: 0 });
  t.set([0, 9, 12, 13], { id: 13, name: 'malachite', type: 'mineral', toggle: 0 });
  // intentional gap
  t.set([20], { id: 20, name: 't', type: 'unknown', toggle: 0 });
  t.set([20, 21], { id: 21, name: 'u', type: 'unknown', toggle: 0 });
  t.set([20, 22], { id: 22, name: 'v', type: 'unknown', toggle: 0 });
  t.set([20, 23], { id: 23, name: 'w', type: 'unknown', toggle: 0 });
  t.set([20, 23, 24], { id: 24, name: 'x', type: 'unknown', toggle: 0 });
  t.set([20, 23, 25], { id: 25, name: 'y', type: 'unknown', toggle: 0 });
  t.set([20, 23, 26], { id: 26, name: 'z', type: 'unknown', toggle: 0 });
  return t;
};

const toggle = (type, num) => {
  return (datum, tree) => {
    if (datum.type === type) datum.toggle = num;
  }
};

const toggleReducer = (type) => {
  return (accumulator, datum, tree) => {
    if (datum.type === type) {
      accumulator = accumulator + datum.toggle;
    }
    return accumulator;
    };
};

const isAnimal = (datum) => datum['type'] === "animal";
const isElectronic = (datum) => datum['type'] === "electronic";
const isMineral = (datum) => datum['type'] === "mineral";
const isNotMineral = (datum) => datum['type'] !== "mineral";
const isNotToggledZero = (datum) => datum['toggle'] !== 0;
const isToggledZero = (datum) => datum['toggle'] === 0;
const isVegetable = (datum) => datum['type'] === "vegetable";

 /* helpful
  [
  [[0], { id: 0, name: 'root', type: 'vegetable', toggle: 0 }],
  [[0, 1], { id: 1, name: 'alsatian', type: 'animal', toggle: 0 }],
  [[0, 1, 2], { id: 2, name: 'beagle', type: 'animal', toggle: 0 }],
  [[0, 1, 3], { id: 3, name: 'cavador', type: 'animal', toggle: 0 }],
  [[0, 1, 4], { id: 4, name: 'daschund', type: 'animal', toggle: 0 }],
  [[0, 5], { id: 5, name: 'eggplant', type: 'vegetable', toggle: 0 }],
  [[0, 5, 6], { id: 6, name: 'fiddlehead', type: 'vegetable', toggle: 0 }],
  [[0, 5, 7], { id: 7, name: 'gnetum', type: 'vegetable', toggle: 0 }],
  [[0, 5, 8], { id: 8, name: 'h', type: 'vegetable', toggle: 0 }],
  [[0, 9], { id: 9, name: 'iron', type: 'mineral', toggle: 0 }],
  [[0, 9, 10], { id: 10, name: 'jadeite', type: 'mineral', toggle: 0 }],
  [[0, 9, 11], { id: 11, name: 'kryptonite', type: 'mineral', toggle: 0 }],
  [[0, 9, 12], { id: 12, name: 'luddite', type: 'mineral', toggle: 0 }],
  [[0, 9, 12, 13], { id: 13, name: 'malachite', type: 'mineral', toggle: 0 }],
    // intentional gap
  [[20], { id: 20, name: 't', type: 'unknown', toggle: 0 }],
  [[20, 21], { id: 21, name: 'u', type: 'unknown', toggle: 0 }],
  [[20, 22], { id: 22, name: 'v', type: 'unknown', toggle: 0 }],
  [[20, 23], { id: 23, name: 'w', type: 'unknown', toggle: 0 }],
  [[20, 23, 24], { id: 24, name: 'x', type: 'unknown', toggle: 0 }],
  [[20, 23, 25], { id: 25, name: 'y', type: 'unknown', toggle: 0 }],
  [[20, 23, 26], { id: 26, name: 'z', type: 'unknown', toggle: 0 }],
  ]
*/


export {
  invalidPathElements,
  isAnimal,
  isElectronic,
  isMineral,
  isNotMineral,
  isNotToggledZero,
  isToggledZero,
  isVegetable,
  prepareDistinctTree,
  toggle,
  toggleReducer
};