import { describe, expect, it } from "vitest";
import {
  invalidPathElements,
  prepareDistinctTree,
  toggle,
  toggleReducer,
} from './tree.utils';

// reduceOf(callbackfn: (accumulator: Accumulator, datum: Datum, tree: Tree) => Accumulator, initialValue: Accumulator, path?: Path, inclusive: boolean): Accumulator;

describe(`The Tree's reduceOf(callbackfn, initialValue, path?, depth) method`, () => {
  let t, cb;
  beforeEach(() => {
    t = prepareDistinctTree();

    // cascade root vegetables to 1, inclusive of root
    t.cascade(toggle('vegetable', 1), 0, true);

    cb = toggleReducer('vegetable');
  });

  it(`should throw when the callback function is not provided, is undefined, or is not a function`, () => {
    expect(() => t.reduceOf()).toThrow();
    let cb2;
    expect(() => t.reduceOf(cb2)).toThrow();
    cb = undefined;
    expect(() => t.reduceOf(cb)).toThrow();
    expect(() => t.reduceOf('foo')).toThrow();
  });

  it(`should throw when initialValue is not provided or undefined`, () => {
    let iv;
    expect(() => t.reduceOf(cb)).toThrow();
    expect(() => t.reduceOf(cb, iv)).toThrow();
    iv = undefined;
    expect(() => t.reduceOf(cb, iv)).toThrow();
  });

  it(`should throw when inclusive is provided and is not boolean`, () => {
    const iv = 0;
    const p = 0;
    expect(() => t.reduceOf(cb, 0, 1, 'a')).toThrow();
    expect(() => t.reduceOf(cb, 0, [0,1], 'a')).toThrow();
  });

  it(`should return initialValue when path is invalid`, () => {
    let v;

    invalidPathElements.forEach((v) => {
      const iv = 0;
      // path elements
      expect(t.reduceOf(cb, iv, v)).toEqual(iv);

      // full paths
      expect(t.reduceOf(cb, iv, [v])).toEqual(iv);
      expect(t.reduceOf(cb, iv, [0, v])).toEqual(iv);
      expect(t.reduceOf(cb, iv, [0, 9, v])).toEqual(iv);
    });
  });

  it(`should propogate the exception when the callback throws`, () => {
    cb = (datum, tree) => { throw new Error("intentional reduceOf callback error") };
    const iv = 0;
    expect(() => t.reduceOf(cb, iv, 1)).toThrow();
    expect(() => t.reduceOf(cb, iv, [0, 1])).toThrow();
    expect(() => t.reduceOf(cb, iv, 1, true)).toThrow();
    expect(() => t.reduceOf(cb, iv, [0, 1], true)).toThrow();
  });

  describe(`when provided a path as a PathElement argument`, () => {
    it(`should return the initialValue when the PathElement does not exist in the Tree`, () => {
      const iv = 0;
      expect(t.reduceOf(cb, iv, 97)).toEqual(iv);
      expect(t.reduceOf(cb, iv, 98, true)).toEqual(iv);
    });

    describe(`when not provided an inclusive argument`, () => {

      it(`should apply the callback to the descendents of the path and return the result`, () => {
        const ret = t.reduceOf(cb, 0, 0);
        expect(ret).toStrictEqual(4);
      });

    });

    describe(`when provided an inclusive argument of false`, () => {

      it(`should apply the callback to the descendents of the path and return the result`, () => {
        const ret = t.reduceOf(cb, 0, 0, false);
        expect(ret).toStrictEqual(4);
      });

    });

    describe(`when provided an inclusive argument of true`, () => {
      it(`should apply the callback to the path's entry with it's descendents and return the result`, () => {
        const ret = t.reduceOf(cb, 0, 0, true);
        expect(ret).toStrictEqual(5);
      });
    });
  });

  describe(`when provided a path as a FullPath argument`, () => {
    it(`should return the initialValue when the FullPath does not exist in the Tree`, () => {
      const iv = 0;
      expect(t.reduceOf(cb, iv, [97])).toEqual(iv);
      expect(t.reduceOf(cb, iv, [98], true)).toEqual(iv);
      expect(t.reduceOf(cb, iv, [0, 5, 97])).toEqual(iv);
      expect(t.reduceOf(cb, iv, [0, 5, 98], true)).toEqual(iv);
    });

    describe(`when not provided an inclusive argument`, () => {
      it(`should apply the callback to the descendents of the path and return the result`, () => {
        const ret = t.reduceOf(cb, 0, [0]);
        expect(ret).toStrictEqual(4);
      });

    });
    describe(`when provided an inclusive argument of false`, () => {
      it(`should apply the callback to the descendents of the path and return the result`, () => {
        const ret = t.reduceOf(cb, 0, [0], false);
        expect(ret).toStrictEqual(4);
      });

    });
    describe(`when provided an inclusive argument of true`, () => {
      it(`should apply the callback to the path's entry and it's descendents and return the result`, () => {
        const ret = t.reduceOf(cb, 0, [0], true);
        expect(ret).toStrictEqual(5);
      });
    });
  });

  describe(`when not provided a path or path is undefined`, () => {
    it(`should apply the callback to all entries and return the result`, () => {
      const iv = 0;
      const ret = t.reduceOf(cb, 0);
      expect(ret).toStrictEqual(5);
    });
  
  });

});
