import { describe, expect, it } from "vitest";
import { Tree } from "../tree";

describe(`The Tree's clear() method,`, () => {

  it(`should remove all existing nodes`, () => {
    const t = new Tree();
    t.set([0], 'root');
    t.set([0,1], 'a');
    t.set([0,2], 'b');
    t.set([0,3], 'c');
    t.set([0,4], 'd');
    t.set([0,5], 'e');
    expect(t.size).toBe(6);

    t.clear();
    expect(t.size).toBe(0);
  });
});
