import { describe, expect, it } from "vitest";
import { invalidPathElements, prepareDistinctTree } from './tree.utils';
import { Tree } from "../tree";

// valuesOf(path?:Path, depth?: number): Values;
describe(`The Tree's valuessOf( path, depth ) method`, () => {
  let t;
  beforeEach(() => {
    t = prepareDistinctTree();
  });

  /*
  it(`should return undefined when the path is not provided or undefined`, () => {
    expect(t.valuesOf()).toBeUndefined();
    expect(t.valuesOf(undefined)).toBeUndefined();
    const u = undefined;
    expect(t.valuesOf(u)).toBeUndefined();
  });
*/

  it(`should return undefined when the path is invalid`, () => {
    let v;
    invalidPathElements.forEach((v) => {
      // path elements
      expect(t.entriesOf(v)).toBeUndefined();

      // full paths
      expect(t.valuesOf([v])).toBeUndefined();
      expect(t.valuesOf([0, v])).toBeUndefined();
      expect(t.valuesOf([0, 9, v])).toBeUndefined();
    });
  });

  it(`should return undefined when the path is not found in the Tree`, () => {
    expect(() => t.valuesOf(97)).not.toThrow();
    expect(t.valuesOf(98)).toBeUndefined();
    expect(t.valuesOf([0, 99])).toBeUndefined();
  });

  it(`should return undefined when the path is found but the depth is not an integer`, () => {
    expect(t.valuesOf(5, 'a')).toBeUndefined();
    expect(t.valuesOf([0, 5], 'a')).toBeUndefined();
  })

  it(`should return undefined when the path is found but the depth is a negative integer`, () => {
    expect(t.valuesOf(5, -1)).toBeUndefined();
    expect(t.valuesOf([0, 5], -1)).toBeUndefined();
  })

  it(`should return undefined when the depth is greater than 9007199254740991 (Number.MAX_SAFE_INTEGER)`, () => {
    const n = 9007199254740991 + 1;
    expect(t.valuesOf(5, n)).toBeUndefined();
    expect(t.valuesOf([0, 5], n)).toBeUndefined();
  })

  describe(`when provided a path`, () => {
    it(`should return the value for the path with its descendents when no depth is provided`, () => {
      let actual, expected;
      expected = [
        { id: 5, name: 'eggplant', type: 'vegetable', toggle: 0 },
        { id: 6, name: 'fiddlehead', type: 'vegetable', toggle: 0 },
        { id: 7, name: 'gnetum', type: 'vegetable', toggle: 0 },
        { id: 8, name: 'h', type: 'vegetable', toggle: 0 },
      ];

      actual = t.valuesOf(5);
      expect(actual).toStrictEqual(expected);
  
      actual = t.valuesOf([0, 5]);
      expect(actual).toStrictEqual(expected);
    });
  
    it(`should return the value for the path when a depth of zero is provided`, () => {
      let actual, expected;
      expected = [
        { id: 5, name: 'eggplant', type: 'vegetable', toggle: 0 }
      ];
  
      actual = t.valuesOf(5, 0);
      expect(actual).toStrictEqual(expected);
  
      actual = t.valuesOf([0, 5], 0);
      expect(actual).toStrictEqual(expected);
    });
  
  
    it(`should return the value for the path with its descendents when a valid depth is provided`, () => {
  
      let actual, expected;
      expected = [
        { id: 5, name: 'eggplant', type: 'vegetable', toggle: 0 },
        { id: 6, name: 'fiddlehead', type: 'vegetable', toggle: 0 },
        { id: 7, name: 'gnetum', type: 'vegetable', toggle: 0 },
        { id: 8, name: 'h', type: 'vegetable', toggle: 0 },
      ];
      actual = t.valuesOf(5, 1);
      expect(actual).toStrictEqual(expected);

      actual = t.valuesOf([0, 5], 1);
      expect(actual).toStrictEqual(expected);

      expected = [
        { id: 20, name: 't', type: 'unknown', toggle: 0 },
        { id: 21, name: 'u', type: 'unknown', toggle: 0 },
        { id: 22, name: 'v', type: 'unknown', toggle: 0 },
        { id: 23, name: 'w', type: 'unknown', toggle: 0 },
        { id: 24, name: 'x', type: 'unknown', toggle: 0 },
        { id: 25, name: 'y', type: 'unknown', toggle: 0 },
        { id: 26, name: 'z', type: 'unknown', toggle: 0 },
      ];
      actual = t.valuesOf(20, 2);
      expect(actual).toStrictEqual(expected);
  
      actual = t.valuesOf([0, 20], 2);
      expect(actual).toStrictEqual(expected);

      actual = t.valuesOf(20, 3);
      expect(actual).toStrictEqual(expected);
  
      actual = t.valuesOf([0, 20], 3);
      expect(actual).toStrictEqual(expected);
    });
  });

  describe(`when not provided a path`, () => {

    describe(`when not provided a depth`, () => {
      it(`should return all values`, () => {
        let actual, expected;
        expected = [
          { id: 0, name: 'root', type: 'vegetable', toggle: 0 },
          { id: 1, name: 'alsatian', type: 'animal', toggle: 0 },
          { id: 2, name: 'beagle', type: 'animal', toggle: 0 },
          { id: 3, name: 'cavador', type: 'animal', toggle: 0 },
          { id: 4, name: 'daschund', type: 'animal', toggle: 0 },
          { id: 5, name: 'eggplant', type: 'vegetable', toggle: 0 },
          { id: 6, name: 'fiddlehead', type: 'vegetable', toggle: 0 },
          { id: 7, name: 'gnetum', type: 'vegetable', toggle: 0 },
          { id: 8, name: 'h', type: 'vegetable', toggle: 0 },
          { id: 9, name: 'iron', type: 'mineral', toggle: 0 },
          { id: 10, name: 'jadeite', type: 'mineral', toggle: 0 },
          { id: 11, name: 'kryptonite', type: 'mineral', toggle: 0 },
          { id: 12, name: 'luddite', type: 'mineral', toggle: 0 },
          { id: 13, name: 'malachite', type: 'mineral', toggle: 0 },
            // intentional gap
          { id: 20, name: 't', type: 'unknown', toggle: 0 },
          { id: 21, name: 'u', type: 'unknown', toggle: 0 },
          { id: 22, name: 'v', type: 'unknown', toggle: 0 },
          { id: 23, name: 'w', type: 'unknown', toggle: 0 },
          { id: 24, name: 'x', type: 'unknown', toggle: 0 },
          { id: 25, name: 'y', type: 'unknown', toggle: 0 },
          { id: 26, name: 'z', type: 'unknown', toggle: 0 },
        ];

        actual = t.valuesOf();
        expect(actual).toStrictEqual(expected);
      });
    })

    describe(`when provided a depth`, () => {
      it(`should return all values`, () => {
        let actual, expected;
        expected = [
          { id: 0, name: 'root', type: 'vegetable', toggle: 0 },
          { id: 1, name: 'alsatian', type: 'animal', toggle: 0 },
          { id: 2, name: 'beagle', type: 'animal', toggle: 0 },
          { id: 3, name: 'cavador', type: 'animal', toggle: 0 },
          { id: 4, name: 'daschund', type: 'animal', toggle: 0 },
          { id: 5, name: 'eggplant', type: 'vegetable', toggle: 0 },
          { id: 6, name: 'fiddlehead', type: 'vegetable', toggle: 0 },
          { id: 7, name: 'gnetum', type: 'vegetable', toggle: 0 },
          { id: 8, name: 'h', type: 'vegetable', toggle: 0 },
          { id: 9, name: 'iron', type: 'mineral', toggle: 0 },
          { id: 10, name: 'jadeite', type: 'mineral', toggle: 0 },
          { id: 11, name: 'kryptonite', type: 'mineral', toggle: 0 },
          { id: 12, name: 'luddite', type: 'mineral', toggle: 0 },
          { id: 13, name: 'malachite', type: 'mineral', toggle: 0 },
            // intentional gap
          { id: 20, name: 't', type: 'unknown', toggle: 0 },
          { id: 21, name: 'u', type: 'unknown', toggle: 0 },
          { id: 22, name: 'v', type: 'unknown', toggle: 0 },
          { id: 23, name: 'w', type: 'unknown', toggle: 0 },
          { id: 24, name: 'x', type: 'unknown', toggle: 0 },
          { id: 25, name: 'y', type: 'unknown', toggle: 0 },
          { id: 26, name: 'z', type: 'unknown', toggle: 0 },
        ];

        actual = t.valuesOf(undefined, 0);
        expect(actual).toStrictEqual(expected);
      });
    });
  });


});