import { beforeEach, describe, expect, it } from "vitest";
import {
  invalidPathElements,
  prepareDistinctTree,
} from './tree.utils';


// prune(path: Path): boolean;

describe(`The Tree's prune( path ) method`, () => {

  let t;
  beforeEach(() => {
    t = prepareDistinctTree();
  });

  it(`should return false when the path is not provided`, () => {
    expect(t.size).toBe(21);

    const actual = t.prune();
    expect(actual).toBeFalsy();
    expect(t.size).toBe(21);
  });

  it(`should return false when the provided path does not exist`, () => {
    let actual;
    expect(t.size).toBe(21);

    actual = t.prune([99]);
    expect(actual).toBeFalsy();
    expect(t.size).toBe(21);

    actual = t.prune([0,8,10]);
    expect(actual).toBeFalsy();
    expect(t.size).toBe(21);

    const ifp = ['foo', 'bar', ''];
    expect(() => t.prune(ifp)).not.toThrow();
    expect(() => t.prune([])).not.toThrow();
    actual = t.prune(ifp);
    expect(actual).toBeFalsy();
    expect(t.size).toBe(21);

    actual = t.prune([]);
    expect(actual).toBeFalsy();
    expect(t.size).toBe(21);

    actual = t.prune('missing_branch');
    expect(actual).toBeFalsy();
    expect(t.size).toBe(21);
  });

  it(`should return false when provided a path with at least one invalid element`, () => {
    const u = undefined;
    let v;
    invalidPathElements.forEach((v) => {
      // path elements
      expect(t.prune(v)).toBeFalsy();

      // full paths
      expect(t.prune([v])).toBeFalsy();
      expect(t.prune([0, v])).toBeFalsy();
      expect(t.prune([0, 9, v])).toBeFalsy();
    });

    expect(t.prune(u)).toBeFalsy();

    // full paths
    expect(t.prune([u])).toBeFalsy();
    expect(t.prune([0, u])).toBeFalsy();
    expect(t.prune([0, 9, u])).toBeFalsy();

    expect(t.size).toBe(21); // nothing was removed
  });

  it(`should return true when the provided path exists`, () => {
    let actual;
    expect(t.size).toBe(21);

    actual = t.prune([0,9,12]); // removed 'luddite' & 'malachite'
    expect(actual).toBeTruthy();
    expect(t.size).toBe(19);

    actual = t.prune(26); // removed 'z'
    expect(actual).toBeTruthy();
    expect(t.size).toBe(18);
  });

  it(`should remove the tree element when the provided path exists`, () => {
    let actual;
    expect(t.size).toBe(21);

    actual = t.prune([0,9,12]); // 'luddite' & 'malachite'
    expect(actual).toBeTruthy();
    expect(t.has([0,9,12])).toBeFalsy();
    expect(t.has(13)).toBeFalsy();
    expect(t.size).toBe(19);

    actual = t.prune(26); // 'z'
    expect(actual).toBeTruthy();
    expect(t.size).toBe(18);
    expect(t.has(26)).toBeFalsy();
  });

  it(`should remove descendents when the provided path exists`, () => {
    let actual;
    expect(t.size).toBe(21);

    actual = t.prune([0,1]); // a,b,c,d
    expect(t.has([0,1])).toBeFalsy();
    expect(t.has([0,1,2])).toBeFalsy();
    expect(t.has([0,1,3])).toBeFalsy();
    expect(t.has([0,1,4])).toBeFalsy();
    expect(t.size).toBe(17);

    actual = t.prune(9); // ijklm
    expect(t.has([0,9])).toBeFalsy();
    expect(t.has([0,9,10])).toBeFalsy();
    expect(t.has([0,9,11])).toBeFalsy();
    expect(t.has([0,9,12])).toBeFalsy();
    expect(t.has([0,9,12,13])).toBeFalsy();
    expect(t.size).toBe(12);
  });

});