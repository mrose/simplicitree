import { describe, expect, it } from "vitest";
import { invalidPathElements, prepareDistinctTree } from './tree.utils';

// toNestedValuesOf(path?: Path): NestedValues;

describe(`The Tree's toNestedValuesOf(path?: Path) method`, () => {
  let t;
  beforeEach(() => {
    t = prepareDistinctTree();
  });

  describe(`when provided a path`, () => {
    it(`should return undefined when the path is invalid`, () => {
      let v;
      invalidPathElements.forEach((v) => {
        // path elements
        expect(t.toNestedValuesOf(v)).toBeUndefined();
  
        // full paths
        expect(t.toNestedValuesOf([v])).toBeUndefined();
        expect(t.toNestedValuesOf([0, v])).toBeUndefined();
        expect(t.toNestedValuesOf([0, 9, v])).toBeUndefined();
      });
    });
  
    it(`should return undefined when the path is not found in the Tree`, () => {
      expect(() => t.toNestedValuesOf(97)).not.toThrow();
      expect(t.toNestedValuesOf(98)).toBeUndefined();
      expect(t.toNestedValuesOf([0, 99])).toBeUndefined();
    });

    it(`should return an array of a single value with no descendents when the node of the path has no descendents`, () => {
      let actual, expected;
  
      expected = [
        [{ id: 4, name: 'daschund', type: "animal", toggle: 0 },
          []
        ],
      ];

      actual = t.toNestedValuesOf(4);
      expect(actual).toMatchObject(expected);

      // fullPath notation
      actual = t.toNestedValuesOf([0, 1, 4]);
      expect(actual).toMatchObject(expected);
    });
  
    it(`should return an array of a single value with descendents when the node of the path has descendents`, () => {
      let actual, expected;
  
      expected = [
        [{ id: 1, name: 'alsatian', type: "animal", toggle: 0 },
          [
            [{ id: 2, name: 'beagle', type: "animal", toggle: 0 },
              []
            ],
            [{ id: 3, name: 'cavador', type: "animal", toggle: 0 },
              []
            ],
            [{ id: 4, name: 'daschund', type: "animal", toggle: 0 },
              []
            ],
          ],
        ]
      ];
      actual = t.toNestedValuesOf(1);
      expect(actual).toMatchObject(expected);

      actual = t.toNestedValuesOf([0, 1]);
      expect(actual).toMatchObject(expected);
    });      
  });


  describe(`when not provided a path`, () => {
    it(`should return an empty array when the tree is empty`, () => {
      t.clear();
      let actual, expected;

      expected = [];
      actual = t.toNestedValuesOf();
      expect(actual).toStrictEqual(expected);
    });

    it(`should return an array of all values with their descendents when the node of the path is the root`, () => {
      let actual, expected;
  
      expected = [
        [{ id: 0, name: 'root', type: "vegetable", toggle: 0 },
          [
            // animals
            [{ id: 1, name: 'alsatian', type: "animal", toggle: 0 },
              [
                [{ id: 2, name: 'beagle', type: "animal", toggle: 0 },
                  []
                ],
                [{ id: 3, name: 'cavador', type: "animal", toggle: 0 },
                  []
                ],
                [{ id: 4, name: 'daschund', type: "animal", toggle: 0 },
                  []
                ],
              ],
            ],
            // vegetables
            [{ id: 5, name: 'eggplant', type: "vegetable", toggle: 0 },
              [
                [{ id: 6, name: 'fiddlehead', type: "vegetable", toggle: 0 },
                  [],
                ],
                [{ id: 7, name: 'gnetum', type: "vegetable", toggle: 0 },
                  [],
                ],
                [{ id: 8, name: 'h', type: "vegetable", toggle: 0 },
                  [],
                ],
              ],
            ],
            // minerals
            [{ id: 9, name: 'iron', type: "mineral", toggle:0 },
              [
                [{ id: 10, name: 'jadeite', type: "mineral", toggle: 0 },
                  [],
                ],
                [{ id: 11, name: 'kryptonite', type: "mineral", toggle:0 },
                  [],
                ],
                [{ id: 12, name: 'luddite', type: "mineral", toggle:0 },
                  [
                    [{ id: 13, name: 'malachite', type: 'mineral', toggle: 0 },
                      [],
                    ],
                  ],
                ],
              ],
            ],
          ],
        ],
        [
          { id: 20, name: 't', type: "unknown", toggle: 0 },
          [
            [{ id: 21, name: 'u', type: 'unknown', toggle: 0 },
              [],
            ],
            [{ id: 22, name: 'v', type: 'unknown', toggle: 0 },
              [],
            ],
            [{ id: 23, name: 'w', type: 'unknown', toggle: 0 },
              [
                [ { id: 24, name: 'x', type: 'unknown', toggle: 0 },
                  []
                ],
                [ { id: 25, name: 'y', type: 'unknown', toggle: 0 },
                  []
                ],
                [ { id: 26, name: 'z', type: 'unknown', toggle: 0 },
                  []
                ],
              ],
            ],          
          ]
        ]
      ];

      actual = t.toNestedValuesOf();
      expect(actual).toMatchObject(expected);
    });
  });
});
