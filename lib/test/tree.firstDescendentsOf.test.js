import { describe, expect, it } from "vitest";
import {
  invalidPathElements,
  isAnimal,
  isElectronic,
  isMineral,
  isNotMineral,
  isNotToggledZero,
  isToggledZero,
  isVegetable,
  prepareDistinctTree,
} from './tree.utils';

import { Tree } from "../tree";

//   firstDescendentsOf(path?: Path): Values;
describe(`The Tree's firstDescendentsOf( path ) method`, () => {
  let t;
  beforeEach(() => {
    t = prepareDistinctTree();
  });

  it(`should return undefined when path is invalid`, () => {
    const u = undefined;
    let v;
    invalidPathElements.forEach((v) => {
      // path elements
      expect(t.firstDescendentsOf(v)).toBeUndefined();

      // full paths
      expect(t.firstDescendentsOf([v])).toBeUndefined();
      expect(t.firstDescendentsOf([0, v])).toBeUndefined();
      expect(t.firstDescendentsOf([0, 9, v])).toBeUndefined();
    });

    // path elements
    expect(t.firstDescendentsOf(u)).toBeUndefined();

    // full paths
    expect(t.firstDescendentsOf([u])).toBeUndefined();
    expect(t.firstDescendentsOf([0,u])).toBeUndefined();
    expect(t.firstDescendentsOf([0, 9, u])).toBeUndefined();
  });

  it(`should return undefined when the path does not exist in the Tree`, () => {
    expect(() => t.firstDescendentsOf(97)).not.toThrow();
    expect(t.firstDescendentsOf(98)).toBeUndefined();
    expect(t.firstDescendentsOf([0, 99])).toBeUndefined();
  });

  it(`should return an empty array when no first descendents of the path exist`, () => {
    expect(t.firstDescendentsOf(4)).toStrictEqual([]);
    expect(t.firstDescendentsOf([0, 1, 4])).toStrictEqual([]);
  });

  it(`should return the first descendents of the path as Values`, () => {
    let actual, expected;

    actual = t.firstDescendentsOf(1);

    expected = [
      { id: 2, name: 'beagle', type: 'animal', toggle: 0 },
      { id: 3, name: 'cavador', type: 'animal', toggle: 0 },
      { id: 4, name: 'daschund', type: 'animal', toggle: 0 },
    ];
    expect(actual).toStrictEqual(expected);

    actual = t.firstDescendentsOf([0,9]);
    expected = [
      { id: 10, name: 'jadeite', type: 'mineral', toggle: 0 },
      { id: 11, name: 'kryptonite', type: 'mineral', toggle: 0 },
      { id: 12, name: 'luddite', type: 'mineral', toggle: 0 },
    ];
    expect(actual).toStrictEqual(expected);
  });

});
