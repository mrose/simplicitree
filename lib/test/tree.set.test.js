import { describe, expect, it } from "vitest";
import { Tree } from "../tree";

describe(`The Tree's set( key, value ) method`, () => {

  it(`should throw when the key is not an array`, () => {
    const t = new Tree();
    const f = function () { return 'foo' };
    const m = new Map();
    const n = null;
    const re = new RegExp("ab+c", "i");
    const s = "myString";
    const set = new Set();
    const u = undefined;

    expect(() => t.set(f,   'value')).toThrow();
    expect(() => t.set(m,   'value')).toThrow();
    expect(() => t.set(n,   'value')).toThrow();
    expect(() => t.set(re,  'value')).toThrow();
    expect(() => t.set(s,   'value')).toThrow();
    expect(() => t.set(set, 'value')).toThrow();
    expect(() => t.set(u,   'value')).toThrow();
  });

  // key must be a full path with valid elements
  it(`should throw when the key array contains invalid elements`, () => {
    const t = new Tree();

    const f = function () { return 'foo' };
    const m = new Map();
    const n = null;
    const re = new RegExp("ab+c", "i");
    const set = new Set();
    const u = undefined;

    expect(() => t.set([], 'b')).toThrow();
    expect(() => t.set([m], 'b')).toThrow();
    expect(() => t.set([n], 'b')).toThrow();
    expect(() => t.set([re], 'b')).toThrow();
    expect(() => t.set([set], 'b')).toThrow();
    expect(() => t.set([u], 'b')).toThrow();
  });

  it(`should set when keys are valid arrays`, () => {
    const t = new Tree();
    let actual, expected, key1, key2;

    // NOTE: array keys are set by REFERENCE, not by VALUE

    key1 = ['root'];
    t.set(key1, { id: 'root', name: 'oak' });

    key2 = ['root', 'a'];
    t.set(key2, { id: 'a', name: 'albert' });

    expected = [
      [['root'], { id: 'root', name: 'oak' }],
      [['root', 'a'], { id: 'a', name: 'albert' }]
    ]
    actual = [...t.entries()];
    expect(actual).toStrictEqual(expected);
  });

  it(`should set an unprovided or undefined value to the Tree's defaultDatum`, () => {
    const t = new Tree();
    const rk = [0];
    let actual, expected;
    t.set(rk, undefined);
    actual = [...t.entries()];
    expected = [
      [[0], null],
    ];
    expect(actual).toStrictEqual(expected);
  });

  it(`should return the Tree when successful`, () => {
    const t = new Tree();
    const ret = t.set(['root'], 'rootDatum');
    expect(ret).toBeInstanceOf(Tree);
    expect(ret).toBeInstanceOf(Map);
  });

});
