import { describe, expect, it } from "vitest";
import { invalidPathElements, prepareDistinctTree } from './tree.utils';


// keysOf(path?:Path, depth?: number): Keys;

describe(`The Tree's keysOf( path?, depth? ) method`, () => {
  let t;
  beforeEach(() => {
    t = prepareDistinctTree();
  });

  it(`should return undefined when the path is invalid`, () => {
    let v;
    invalidPathElements.forEach((v) => {
      // path elements
      expect(t.keysOf(v)).toBeUndefined();

      // full paths
      expect(t.keysOf([v])).toBeUndefined();
      expect(t.keysOf([0, v])).toBeUndefined();
      expect(t.keysOf([0, 9, v])).toBeUndefined();
    });
  });

  it(`should return undefined when the path is not found in the Tree`, () => {
    expect(() => t.keysOf(97)).not.toThrow();
    expect(t.keysOf(98)).toBeUndefined();
    expect(t.keysOf([0, 99])).toBeUndefined();
  });

  it(`should return undefined when the path is found but the depth is not an integer`, () => {
    expect(t.keysOf(5, 'a')).toBeUndefined();
    expect(t.keysOf([0, 5], 'a')).toBeUndefined();
  })

  it(`should return undefined when the path is found but the depth is a negative integer`, () => {
    expect(t.keysOf(5, -1)).toBeUndefined();
    expect(t.keysOf([0, 5], -1)).toBeUndefined();
  })

  it(`should return undefined when the depth is greater than 9007199254740991 (Number.MAX_SAFE_INTEGER)`, () => {
    const n = 9007199254740991 + 1;
    expect(t.keysOf(5, n)).toBeUndefined();
    expect(t.keysOf([0, 5], n)).toBeUndefined();
  })

  describe(`when provided a path`, () => {
    it(`should return the key for the path and all its descendents when no depth is provided`, () => {
      let actual, expected;
      expected = [
        [0, 5],
        [0, 5, 6],
        [0, 5, 7],
        [0, 5, 8],
      ];
  
      actual = t.keysOf(5);
      expect(actual).toStrictEqual(expected);
  
      actual = t.keysOf([0, 5]);
      expect(actual).toStrictEqual(expected);
    });
  
    it(`should return the key for the path when a depth of zero is provided`, () => {
      let actual, expected;
      expected = [
        [0, 5],
      ];
  
      actual = t.keysOf(5, 0);
      expect(actual).toStrictEqual(expected);
  
      actual = t.keysOf([0, 5], 0);
      expect(actual).toStrictEqual(expected);
    });
  
  
    it(`should return the key for the path with its descendents when a positive integer depth is provided`, () => {
      t.set([0, 5, 8, 14], { id:14, name:'n', type: "unknown", toggle: 0 });
      t.set([0, 5, 8, 15], { id:15, name:'o', type: "unknown", toggle: 0 });
      t.set([0, 5, 8, 16], { id:16, name:'p', type: "unknown", toggle: 0 });
  
      let actual, expected;
      expected = [
        [0, 5],
        [0, 5, 6],
        [0, 5, 7],
        [0, 5, 8],
      ];
  
      actual = t.keysOf(5, 1);
      expect(actual).toStrictEqual(expected);
  
      actual = t.keysOf([0, 5], 1);
      expect(actual).toStrictEqual(expected);
  
      expected = [
        [0, 5],
        [0, 5, 6],
        [0, 5, 7],
        [0, 5, 8],
        [0, 5, 8, 14],
        [0, 5, 8, 15],
        [0, 5, 8, 16],
      ];
      actual = t.keysOf(5, 2);
      expect(actual).toStrictEqual(expected);
  
      actual = t.keysOf([0, 5], 2);
      expect(actual).toStrictEqual(expected);

      actual = t.keysOf(5, 3);
      expect(actual).toStrictEqual(expected);
  
      actual = t.keysOf([0, 5], 3);
      expect(actual).toStrictEqual(expected);
      
    });
  });
 
  describe(`when not provided a path`, () => {

    describe(`when not provided a depth`, () => {
      it(`should return all keys`, () => {
        let actual, expected;
        expected = [
          [0],
          [0,1],
          [0,1,2],
          [0,1,3],
          [0,1,4],
          [0, 5],
          [0, 5, 6],
          [0, 5, 7],
          [0, 5, 8],
          [0,9],
          [0,9,10],
          [0,9,11],
          [0,9,12],
          [0,9,12,13],
          // intentional gap
          [20],
          [20, 21],
          [20, 22],
          [20, 23],
          [20, 23, 24],
          [20, 23, 25],
          [20, 23, 26],
        ];
  
        actual = t.keysOf();
        expect(actual).toStrictEqual(expected);
      });            
    })

    describe(`when provided a depth`, () => {
      it(`should return all entries`, () => {
        let actual, expected;
        expected = [
          [0],
          [0,1],
          [0,1,2],
          [0,1,3],
          [0,1,4],
          [0, 5],
          [0, 5, 6],
          [0, 5, 7],
          [0, 5, 8],
          [0,9],
          [0,9,10],
          [0,9,11],
          [0,9,12],
          [0,9,12,13],
          // intentional gap
          [20],
          [20, 21],
          [20, 22],
          [20, 23],
          [20, 23, 24],
          [20, 23, 25],
          [20, 23, 26],
        ];
  
        actual = t.keysOf();
        expect(actual).toStrictEqual(expected);
      });
    });
  });

});