import { describe, expect, it } from "vitest";
import { Tree } from "../tree";

// forEach = (callbackfn: (datum: Datum, path: FullPath, tree: Tree<FullPath, Datum>) => void): void

describe(`The Tree's forEach(callbackfn) method`, () => {
  let cb, t;
  beforeEach(() => {
    t = new Tree();
    t.set([0], { id: 0, name: 'root', type: "vegetable", toggle:0 });
    t.set([0, 1], { id: 1, name: 'alsatian', type: "animal", toggle:0 });
    t.set([0, 1, 2], { id: 2, name: 'beagle', type: "animal", toggle:0 });
    t.set([0, 1, 3], { id: 3, name: 'cavador', type: "animal", toggle:0 });
    t.set([0, 1, 4], { id: 4, name: 'daschund', type: "animal", toggle:0 });
    t.set([0, 5], { id: 5, name: 'eggplant', type: "vegetable", toggle:0 });
    t.set([0, 5, 6], { id: 6, name: 'fiddlehead', type: "vegetable", toggle:0 });
    t.set([0, 5, 7], { id: 7, name: 'gnetum', type: "vegetable", toggle:0 });
    t.set([0, 5, 8], { id: 8, name: 'h', type: "vegetable", toggle:0 });
    t.set([0, 9], { id: 9, name: 'iron', type: "mineral", toggle:0 });
    t.set([0, 9, 10], { id: 10, name: 'jadeite', type: "mineral", toggle:0 });
    t.set([0, 9, 11], { id: 11, name: 'kryptonite', type: "mineral", toggle:0 });
    t.set([0, 9, 12], { id: 12, name: 'luddite', type: "mineral", toggle:0 });
    t.set([0, 13], { id: 13, name: 'mud', type: "unknown", toggle:0 });
  });

  it(`should throw when callbackfn is not provided, is undefined, or is not a function`, () => {
    expect(() => t.forEach()).toThrow();
    expect(() => t.forEach(cb)).toThrow();
    cb = undefined;
    expect(() => t.forEach(cb)).toThrow();
    expect(() => t.forEach('foo')).toThrow();
  });

  it(`should propogate the exception when the callback throws`, () => {
    cb = (datum, path, tree) => { throw new Error("intentional forEach callback error") };
    expect(() => t.forEach(cb)).toThrow();
  });

  it(`should apply the callback to all elements of the Tree and return void`, () => {
    cb = (datum, path, tree) => {
      datum.firstDescendents = tree.firstDescendentsOf(path);
    };
    const ret = t.forEach(cb);
    expect(ret).toBeUndefined();
    const everyOfCb = (datum) => Array.isArray(datum.firstDescendents);
    expect(t.everyOf(everyOfCb, 0)).toBeTruthy();
  });

});
