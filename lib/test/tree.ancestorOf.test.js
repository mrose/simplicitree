import { describe, expect, it } from "vitest";
import { Tree } from "../tree";

// ancestorOf(path: Path): Datum | undefined;
describe(`The Tree's ancestorOf( path ) method,`, () => {
  let t;
  beforeEach(() => {
    t = new Tree();
    t.set([0], 'root');
    t.set([0, 1], 'a');
    t.set([0, 1, 2], 'b');
    t.set([0, 1, 3], 'c');
    t.set([0, 1, 4], 'd');
    t.set([0, 5], 'e');
    t.set([0, 5, 6], 'f');
    t.set([0, 5, 7], 'g');
    t.set([0, 5, 8], 'h');
    t.set([0, 9], 'i');
    t.set([0, 9, 10], 'j');
    t.set([0, 9, 11], 'k');
    t.set([0, 9, 12], 'l');
    t.set([0, 13], 'm');
  });

  it(`should return undefined when the path is not provided or undefined`, () => {
    expect(t.ancestorOf()).toBeUndefined();
    expect(t.ancestorOf(undefined)).toBeUndefined();
    const u = undefined;
    expect(t.ancestorOf(u)).toBeUndefined();
  });

  it(`should return undefined when the path is invalid`, () => {
    const a = [];
    const f = function () { return 'foo' };
    const m = new Map();
    const n = null;
    const re = new RegExp("ab+c", "i");
    const s = new Set();
    const u = undefined;

    // path elements
    expect(t.ancestorOf(a)).toBeUndefined();
    expect(t.ancestorOf(f)).toBeUndefined();
    expect(t.ancestorOf(m)).toBeUndefined();
    expect(t.ancestorOf(n)).toBeUndefined();
    expect(t.ancestorOf(s)).toBeUndefined();
    expect(t.ancestorOf(u)).toBeUndefined();

    // full paths

    expect(t.ancestorOf([a])).toBeUndefined();
    expect(t.ancestorOf([f])).toBeUndefined();
    expect(t.ancestorOf([m])).toBeUndefined();
    expect(t.ancestorOf([n])).toBeUndefined();
    expect(t.ancestorOf([s])).toBeUndefined();
    expect(t.ancestorOf([u])).toBeUndefined();

    expect(t.ancestorOf([0,a])).toBeUndefined();
    expect(t.ancestorOf([0,f])).toBeUndefined();
    expect(t.ancestorOf([0,m])).toBeUndefined();
    expect(t.ancestorOf([0,n])).toBeUndefined();
    expect(t.ancestorOf([0,s])).toBeUndefined();
    expect(t.ancestorOf([0,u])).toBeUndefined();

    expect(t.ancestorOf([0, 9, n])).toBeUndefined();

  });

  it(`should return undefined when the path is not found in the Tree`, () => {
    expect(() => t.ancestorOf(97)).not.toThrow();
    expect(t.ancestorOf(98)).toBeUndefined();
    expect(t.ancestorOf([0, 99])).toBeUndefined();
  });

  it(`should return undefined when the path is a root element`, () => {
    expect(t.ancestorOf(0)).toBeUndefined();
    expect(t.ancestorOf([0])).toBeUndefined();

    t.set([50], 'qqq');
    t.set([50, 1], 'rrr');
    expect(t.ancestorOf(50)).toBeUndefined();
    expect(t.ancestorOf([50])).toBeUndefined();
  });

  it(`should return the datum associated with the path when the path exists in the Tree`, () => {
    let actual, expected;
    actual = t.ancestorOf(1);
    expected = 'root';
    expect(actual).toStrictEqual(expected);

    actual = t.ancestorOf([0,1]);
    expected = 'root';
    expect(actual).toStrictEqual(expected);

    actual = t.ancestorOf(4);
    expected = 'a';
    expect(actual).toStrictEqual(expected);

    actual = t.ancestorOf([0,1,4]);
    expected = 'a';
    expect(actual).toStrictEqual(expected);

  });

});