import { describe, expect, it } from "vitest";
import { Tree } from "../tree";

describe(`The Tree's get( key ) method,`, () => {

  it(`should throw when provided a key which is not an array and is not undefined`, () => {
    const t = new Tree();
    expect(() => t.get('somestring')).toThrow();
    expect(() => t.get(0)).toThrow();
    expect(() => t.get(42)).toThrow();
    expect(() => t.get(null)).toThrow();
    expect(() => t.get(() => 'foo')).toThrow();
    expect(() => t.get(/ab+c/i)).toThrow();
    expect(() => t.get()).not.toThrow();
  });

  it(`should retrieve values for keys which are arrays`, () => {
    const t = new Tree();
    let actual, expected, key1, key2;

    // NOTE: array keys are set by REFERENCE, not by VALUE

    key1 = ['root'];
    t.set(key1, { id: 'root', name: 'oak' });
    actual = t.get(key1);
    expected = { id: 'root', name: 'oak' };
    expect(actual).toStrictEqual(expected);

    key2 = ['root', 'a', 'b', 'c'];
    t.set(key2, { id: 'c', name: 'caroline' });
    actual = t.get(key2);
    expected = { id: 'c', name: 'caroline' };
    expect(actual).toStrictEqual(expected);

    expected = [
      [key1, { id: 'root', name: 'oak' }],
      [key2, { id: 'c', name: 'caroline' }]
    ];
    actual = [...t.entries()];
    expect(actual).toStrictEqual(expected);

    // note the keys and enjoy the magic of spread
    expected = [
      [['root'], { id: 'root', name: 'oak' }],
      [['root', 'a', 'b', 'c'], { id: 'c', name: 'caroline' }]
    ];
    actual = [...t.entries()];
    expect(actual).toStrictEqual(expected);
  });

  it(`should return undefined when the provided path does not exist`, () => {
    const t = new Tree();
    const ifp = ['foo', 'bar', ''];
    expect(() => t.get(ifp)).not.toThrow();
    expect(() => t.get([])).not.toThrow();
    expect(t.get(ifp)).toBeUndefined();
    expect(t.get([])).toBeUndefined();

    t.set([0], 'root');
    t.set([0,1], 'a');
    t.set([0,2], 'b');
    t.set([0,2,3], 'c');
    expect(t.get([1])).toBeUndefined();
    expect(t.get([2,3])).toBeUndefined();
  });

  it(`should return the value when provided an existing full path`, () => {
    const t = new Tree();

    /*
      NOTE: array keys are set by REFERENCE, not by VALUE
      if you used set(), you already have the key
      if you used setNode() the key is returned to you
      nonetheless retrieval by value is supported anyways
    */
    let actual, key1, key2;
    key1 = ['root'];
    key2 = ['root', 'a', 'b', 'c'];
    t.set(key1, { id: 'root', name: 'oak' });
    t.set(key2, { id: 'c', name: 'caroline' });
    actual = t.get(key1);
    expect(actual).toEqual({ id: 'root', name: 'oak' });
    actual = t.get(key2);
    expect(actual).toEqual({ id: 'c', name: 'caroline' });

    // retrieval by value is supported anyways
    actual = t.get(['root', 'a', 'b', 'c']);
    expect(actual).toEqual({ id: 'c', name: 'caroline' });
  });

  it(`should return undefined when provided an undefined key`, () => {
    const t = new Tree();
    let actual;
    t.set(['root'], { id: 'root', name: 'oak' });
    t.set(['root', 'a'], { id: 'a', name: 'anthony' });
    t.set(['root', 'a', 'b'], { id: 'b', name: 'bettina' });
    t.set(['root', 'a', 'b', 'c'], { id: 'c', name: 'caroline' });

    actual = t.get();
    expect(actual).toBeUndefined();
  });

  it(`should return undefined when provided a non-existing full path`, () => {
    const t = new Tree();
    let actual;
    t.set(['root'], { id: 'root', name: 'oak' });
    t.set(['root', 'a'], { id: 'a', name: 'anthony' });
    t.set(['root', 'a', 'b'], { id: 'b', name: 'bettina' });
    t.set(['root', 'a', 'b', 'c'], { id: 'c', name: 'caroline' });

    actual = t.get(['root', 'x', 'y', 'z']);
    expect(actual).toBeUndefined();
  });

});
