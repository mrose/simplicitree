import { describe, expect, it } from "vitest";
import { Tree } from "../tree";

// cascade(callbackfn: (datum:Datum, tree: Tree<FullPath, Datum>) => void, path: Path, inclusive = false): boolean;

describe(`The Tree's cascade(callbackfn, path, depth ) method`, () => {
  let cb, t;
  beforeEach(() => {
    t = new Tree();
    t.set([0], { id: 0, name: 'root', type: "vegetable", toggle:0 });
    t.set([0, 1], { id: 1, name: 'alsatian', type: "animal", toggle:0 });
    t.set([0, 1, 2], { id: 2, name: 'beagle', type: "animal", toggle:0 });
    t.set([0, 1, 3], { id: 3, name: 'cavador', type: "animal", toggle:0 });
    t.set([0, 1, 4], { id: 4, name: 'daschund', type: "animal", toggle:0 });
    t.set([0, 5], { id: 5, name: 'eggplant', type: "vegetable", toggle:0 });
    t.set([0, 5, 6], { id: 6, name: 'fiddlehead', type: "vegetable", toggle:0 });
    t.set([0, 5, 7], { id: 7, name: 'gnetum', type: "vegetable", toggle:0 });
    t.set([0, 5, 8], { id: 8, name: 'h', type: "vegetable", toggle:0 });
    t.set([0, 9], { id: 9, name: 'iron', type: "mineral", toggle:0 });
    t.set([0, 9, 10], { id: 10, name: 'jadeite', type: "mineral", toggle:0 });
    t.set([0, 9, 11], { id: 11, name: 'kryptonite', type: "mineral", toggle:0 });
    t.set([0, 9, 12], { id: 12, name: 'luddite', type: "mineral", toggle:0 });
    t.set([0, 13], { id: 13, name: 'mud', type: "unknown", toggle:0 });

    cb = (datum, tree) => {
      let { id, name, type, toggle } = datum;
      if (type === 'vegetable') {
        // n.b. since this callback is executing within an iteration cycle
        // it is likely that trying to mutate the current record using
        // tree methods will timeout or fail
        // so mutate in situ is recommended
        datum.toggle = 1;
      }
    };
  });

  it(`should throw (like Map.forEach()) when callbackfn is not provided, is undefined, or is not a function`, () => {
    expect(() => t.cascade()).toThrow();
    cb = undefined;
    expect(() => t.cascade(cb)).toThrow;
    expect(() => t.cascade('foo')).toThrow;
  });

  it(`should return false when path is not provided or undefined`, () => {
    expect(t.cascade(cb)).toBeFalsy();
    expect(t.cascade(cb, undefined)).toBeFalsy();
    const u = undefined;
    expect(t.cascade(cb, u)).toBeFalsy();
  });

  it(`should return false when path is invalid`, () => {
    const a = [];
    const f = function () { return 'foo' };
    const m = new Map();
    const n = null;
    const re = new RegExp("ab+c", "i");
    const s = new Set();
    const u = undefined;

    // path elements
    expect(t.cascade(cb, a)).toBeFalsy();
    expect(t.cascade(cb, f)).toBeFalsy();
    expect(t.cascade(cb, m)).toBeFalsy();
    expect(t.cascade(cb, n)).toBeFalsy();
    expect(t.cascade(cb, s)).toBeFalsy();
    expect(t.cascade(cb, u)).toBeFalsy();

    // full paths
    expect(t.cascade(cb, [a])).toBeFalsy();
    expect(t.cascade(cb, [f])).toBeFalsy();
    expect(t.cascade(cb, [m])).toBeFalsy();
    expect(t.cascade(cb, [n])).toBeFalsy();
    expect(t.cascade(cb, [s])).toBeFalsy();
    expect(t.cascade(cb, [u])).toBeFalsy();

    expect(t.cascade(cb, [0, a])).toBeFalsy();
    expect(t.cascade(cb, [0, f])).toBeFalsy();
    expect(t.cascade(cb, [0, m])).toBeFalsy();
    expect(t.cascade(cb, [0, n])).toBeFalsy();
    expect(t.cascade(cb, [0, s])).toBeFalsy();
    expect(t.cascade(cb, [0, u])).toBeFalsy();

    expect(t.cascade(cb, [0, 9, n])).toBeFalsy();

  });

  it(`should return false when inclusive is provided and is not boolean`, () => {
    expect(t.cascade(cb, 1, 'a')).toBeFalsy();
    expect(t.cascade(cb, [0, 1], 'a')).toBeFalsy();
  });


  it(`should propogate the exception when the callback throws`, () => {
    cb = (datum, tree) => { throw new Error("intentional cascade callback error") };
    expect(() => t.cascade(cb, 0)).toThrow();
    expect(() => t.cascade(cb, [0, 1])).toThrow();
    expect(() => t.cascade(cb, 0, true)).toThrow();
    expect(() => t.cascade(cb, [0, 1], true)).toThrow();
  });


  describe(`when provided a path as a PathElement argument`, () => {
    it(`should return false when the PathElement does not exist in the Tree`, () => {
      expect(t.cascade(cb, 97)).toBeFalsy();
      expect(t.cascade(cb, 98, true)).toBeFalsy();
    });

    describe(`when not provided an inclusive argument`, () => {

      it(`should apply the callback to the descendents of the path and return true`, () => {
        // toggle vegetables to 1, except root
        const isToggled = (datum) => datum['toggle'] === 1;
        const isNotToggled = (datum) => datum['toggle'] !== 1;
        const ret = t.cascade(cb, 0);
        expect(ret).toBeTruthy();
        expect(t.getNode(0).toggle).toStrictEqual(0);
        expect(t.everyOf(isToggled, 5)).toBeTruthy(); // 5 & descendents are veggies
        expect(t.everyOf(isNotToggled, 1)).toBeTruthy(); // animals
        expect(t.everyOf(isNotToggled, 9)).toBeTruthy(); // minerals
        expect(t.getNode(13).toggle).toStrictEqual(0);
      });

    });

    describe(`when provided an inclusive argument of false`, () => {

      it(`should apply the callback to the descendents of the path and return true`, () => {
        // toggle vegetables to 1, except root
        const isToggled = (datum) => datum['toggle'] === 1;
        const isNotToggled = (datum) => datum['toggle'] !== 1;
        const ret = t.cascade(cb, 0);
        expect(ret).toBeTruthy();
        expect(t.getNode(0).toggle).toStrictEqual(0);
        expect(t.everyOf(isToggled, 5)).toBeTruthy(); // 5 & descendents are veggies
        expect(t.everyOf(isNotToggled, 1)).toBeTruthy(); // animals
        expect(t.everyOf(isNotToggled, 9)).toBeTruthy(); // minerals
        expect(t.getNode(13).toggle).toStrictEqual(0);
      });

    });

    describe(`when provided an inclusive argument of true`, () => {
        it(`should apply the callback to the path's node and it's descendents and return true`, () => {
          // toggle vegetables to 1, including root
          const isToggled = (datum) => datum['toggle'] === 1;
          const isNotToggled = (datum) => datum['toggle'] !== 1;
          const ret = t.cascade(cb, 0, true);
          expect(ret).toBeTruthy();
          expect(t.getNode(0).toggle).toStrictEqual(1);
          expect(t.everyOf(isToggled, 5)).toBeTruthy(); // 5 & descendents are veggies
          expect(t.everyOf(isNotToggled, 1)).toBeTruthy(); // animals
          expect(t.everyOf(isNotToggled, 9)).toBeTruthy(); // minerals
          expect(t.getNode(13).toggle).toStrictEqual(0);
          });

    });

  });

  describe(`when provided a path as a FullPath argument`, () => {
    it(`should return false when the FullPath does not exist in the Tree`, () => {
      const cb = (datum, tree) => { return 'roo'; };
      expect(t.cascade(cb, [0, 97])).toBeFalsy();
      expect(t.cascade(cb, [0, 98], true)).toBeFalsy();
    });

    describe(`when not provided an inclusive argument`, () => {
      it(`should apply the callback to the descendents of the path and return true`, () => {
        // toggle vegetables to 1, except root
        const isToggled = (datum) => datum['toggle'] === 1;
        const isNotToggled = (datum) => datum['toggle'] !== 1;
        const ret = t.cascade(cb, [0]);
        expect(ret).toBeTruthy();
        expect(t.getNode(0).toggle).toStrictEqual(0);
        expect(t.everyOf(isToggled, 5)).toBeTruthy(); // 5 & descendents are veggies
        expect(t.everyOf(isNotToggled, 1)).toBeTruthy(); // animals
        expect(t.everyOf(isNotToggled, 9)).toBeTruthy(); // minerals
        expect(t.getNode(13).toggle).toStrictEqual(0);
      });

    });
    describe(`when provided an inclusive argument of false`, () => {
      it(`should apply the callback to the descendents of the path and return true`, () => {
        // toggle vegetables to 1, except root
        const isToggled = (datum) => datum['toggle'] === 1;
        const isNotToggled = (datum) => datum['toggle'] !== 1;
        const ret = t.cascade(cb, [0], false);
        expect(ret).toBeTruthy();
        expect(t.getNode(0).toggle).toStrictEqual(0);
        expect(t.everyOf(isToggled, 5)).toBeTruthy(); // 5 & descendents are veggies
        expect(t.everyOf(isNotToggled, 1)).toBeTruthy(); // animals
        expect(t.everyOf(isNotToggled, 9)).toBeTruthy(); // minerals
        expect(t.getNode(13).toggle).toStrictEqual(0);
      });

    });
    describe(`when provided an inclusive argument of true`, () => {
      it(`should apply the callback to the path's node and it's descendents and return true`, () => {
        // toggle vegetables to 1, including root
        const isToggled = (datum) => datum['toggle'] === 1;
        const isNotToggled = (datum) => datum['toggle'] !== 1;
        const ret = t.cascade(cb, [0], true);
        expect(ret).toBeTruthy();
        expect(t.getNode(0).toggle).toStrictEqual(1);
        expect(t.everyOf(isToggled, 5)).toBeTruthy(); // 5 & descendents are veggies
        expect(t.everyOf(isNotToggled, 1)).toBeTruthy(); // animals
        expect(t.everyOf(isNotToggled, 9)).toBeTruthy(); // minerals
        expect(t.getNode(13).toggle).toStrictEqual(0);
      });
    });
  });
});
