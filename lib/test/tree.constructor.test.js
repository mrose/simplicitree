import { describe, expect, it } from "vitest";
import { Tree } from "../tree";

describe(`Thee Tree constructor`, () => {

  it(`should throw when provided arguments`, () => {
    // the Map constructor permits a single argument which will be converted
    // to map elements. We don't, for now.
    expect(() => t = new Tree('foo')).toThrow();
  });

  it(`should create a tree with some properties`, () => {
    const t = new Tree();
    expect(t).toBeInstanceOf(Tree);
    expect(t).toBeInstanceOf(Map);
    expect(t.defaultDatum).toBeNull();
    expect(() => { t.defaultDatum = 'foo' }).not.toThrow();
    expect(t.depth).toBeTypeOf("number");
    expect(t.depth).toBe(0);
    expect(t.isEmpty).toBeTypeOf("boolean");
    expect(t.isEmpty).toBeTruthy;
    expect(t.size).toBeTypeOf("number");
    expect(t.size).toBe(0);
  });


  it(`should create a Tree which provides methods based on Map
  n.b. some methods have been extended for Tree related execution`, () => {
    const t = new Tree();
    expect(t).toBeInstanceOf(Tree);
    expect(t).toBeInstanceOf(Map);
    expect(t.clear).toBeTypeOf('function');
    expect(t.delete).toBeTypeOf('function');
    expect(t.entries).toBeTypeOf('function');
    expect(t.forEach).toBeTypeOf('function');
    expect(t.get).toBeTypeOf('function');
    expect(t.has).toBeTypeOf('function');
    expect(t.keys).toBeTypeOf('function');
    expect(t.set).toBeTypeOf('function');
    expect(t.values).toBeTypeOf('function');

    // n.b. tree.set overrides map semantics
    const t2 = t.set(['foo'], 'bar');

    expect(t2).toBeInstanceOf(Tree);
    expect(t2).toBeInstanceOf(Map);
    const te = [...t.entries()];
    const expectedEntries = [[['foo'], 'bar']];
    expect(te).toEqual(expectedEntries);
    const tk = [...t.keys()];
    const expectedKeys = [['foo']];
    expect(tk).toEqual(expectedKeys);
    const tv = [...t.values()];
    const expectedValues = ['bar'];
    expect(tv).toEqual(expectedValues);
    const tg = t.get(['foo']);
    const expectedGet = 'bar';
    // FIXME
    //expect(tg).toEqual(expectedGet);
    expect(t.has(['foo'])).toBeTruthy;
    expect(t.size).toBe(1);
    t.clear();
    expect(t.size).toBe(0);
  })

});