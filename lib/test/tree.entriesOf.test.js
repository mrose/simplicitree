import { describe, expect, it } from 'vitest';
import { invalidPathElements, prepareDistinctTree } from './tree.utils';


// entriesOf(path?:Path, depth?: number): Entries;

describe(`The Tree's entriesOf( path?, depth? ) method`, () => {
  let t;
  beforeEach(() => {
    t = prepareDistinctTree();
  });

  it(`should return undefined when the path is invalid`, () => {
    let v;
    invalidPathElements.forEach((v) => {
      // path elements
      expect(t.entriesOf(v)).toBeUndefined();

      // full paths
      expect(t.entriesOf([v])).toBeUndefined();
      expect(t.entriesOf([0, v])).toBeUndefined();
      expect(t.entriesOf([0, 9, v])).toBeUndefined();
    });
  });

  it(`should return undefined when the path is not found in the Tree`, () => {
    expect(() => t.entriesOf(97)).not.toThrow();
    expect(t.entriesOf(98)).toBeUndefined();
    expect(t.entriesOf([0, 99])).toBeUndefined();
  });

  it(`should return undefined when the path is found but the depth is not an integer`, () => {
    expect(t.entriesOf(5, 'a')).toBeUndefined();
    expect(t.entriesOf([0, 5], 'a')).toBeUndefined();
  })

  it(`should return undefined when the path is found but the depth is a negative integer`, () => {
    expect(t.entriesOf(5, -1)).toBeUndefined();
    expect(t.entriesOf([0, 5], -1)).toBeUndefined();
  })

  it(`should return undefined when the depth is greater than 9007199254740991 (Number.MAX_SAFE_INTEGER)`, () => {
    const n = 9007199254740991 + 1;
    expect(t.entriesOf(5, n)).toBeUndefined();
    expect(t.entriesOf([0, 5], n)).toBeUndefined();
  })

  describe(`when provided a path`, () => {
    it(`should return the entry for the path with its descendents when no depth is provided`, () => {
      let actual, expected;
      expected = [
        [[0, 5], { id: 5, name: 'eggplant', type: 'vegetable', toggle: 0 }],
        [[0, 5, 6], { id: 6, name: 'fiddlehead', type: 'vegetable', toggle: 0 }],
        [[0, 5, 7], { id: 7, name: 'gnetum', type: 'vegetable', toggle: 0 }],
        [[0, 5, 8], { id: 8, name: 'h', type: 'vegetable', toggle: 0 }],
      ];
  
      actual = t.entriesOf(5);
      expect(actual).toStrictEqual(expected);
  
      actual = t.entriesOf([0, 5]);
      expect(actual).toStrictEqual(expected);
    });
  
    it(`should return the entry for the path when a depth of zero is provided`, () => {
      let actual, expected;
      expected = [
        [[0, 5], { id: 5, name: 'eggplant', type: 'vegetable', toggle: 0 }]
      ];

      actual = t.entriesOf(5, 0);
      expect(actual).toStrictEqual(expected);
  
      actual = t.entriesOf([0, 5], 0);
      expect(actual).toStrictEqual(expected);
    });
  
  
    it(`should return the entry for the path with its descendents when a positive depth integer is provided`, () => {
      t.set([0, 5, 8, 14], { id:14, name:'n', type: "unknown", toggle: 0 });
      t.set([0, 5, 8, 15], { id:15, name:'o', type: "unknown", toggle: 0 });
      t.set([0, 5, 8, 16], { id:16, name:'p', type: "unknown", toggle: 0 });
  
      let actual, expected;
      expected = [
        [[0, 5], { id: 5, name: 'eggplant', type: 'vegetable', toggle: 0 }],
        [[0, 5, 6], { id: 6, name: 'fiddlehead', type: 'vegetable', toggle: 0 }],
        [[0, 5, 7], { id: 7, name: 'gnetum', type: 'vegetable', toggle: 0 }],
        [[0, 5, 8], { id: 8, name: 'h', type: 'vegetable', toggle: 0 }],
      ];
  
      actual = t.entriesOf(5, 1);
      expect(actual).toStrictEqual(expected);
  
      actual = t.entriesOf([0, 5], 1);
      expect(actual).toStrictEqual(expected);
  
      expected = [
        [[0, 5], { id: 5, name: 'eggplant', type: 'vegetable', toggle: 0 }],
        [[0, 5, 6], { id: 6, name: 'fiddlehead', type: 'vegetable', toggle: 0 }],
        [[0, 5, 7], { id: 7, name: 'gnetum', type: 'vegetable', toggle: 0 }],
        [[0, 5, 8], { id: 8, name: 'h', type: 'vegetable', toggle: 0 }],
        [[0, 5, 8, 14], { id:14, name:'n', type: "unknown", toggle: 0 }],
        [[0, 5, 8, 15], { id:15, name:'o', type: "unknown", toggle: 0 }],
        [[0, 5, 8, 16], { id:16, name:'p', type: "unknown", toggle: 0 }],   
      ];
      actual = t.entriesOf(5, 2);
      expect(actual).toStrictEqual(expected);
  
      actual = t.entriesOf([0, 5], 2);
      expect(actual).toStrictEqual(expected);

      actual = t.entriesOf(5, 3);
      expect(actual).toStrictEqual(expected);
  
      actual = t.entriesOf([0, 5], 3);
      expect(actual).toStrictEqual(expected);

    });   
  });

  describe(`when not provided a path`, () => {

    describe(`when not provided a depth`, () => {
      it(`should return all entries`, () => {
        let actual, expected;
        expected = [
          [[0], { id: 0, name: 'root', type: 'vegetable', toggle: 0 }],
          [[0, 1], { id: 1, name: 'alsatian', type: 'animal', toggle: 0 }],
          [[0, 1, 2], { id: 2, name: 'beagle', type: 'animal', toggle: 0 }],
          [[0, 1, 3], { id: 3, name: 'cavador', type: 'animal', toggle: 0 }],
          [[0, 1, 4], { id: 4, name: 'daschund', type: 'animal', toggle: 0 }],
          [[0, 5], { id: 5, name: 'eggplant', type: 'vegetable', toggle: 0 }],
          [[0, 5, 6], { id: 6, name: 'fiddlehead', type: 'vegetable', toggle: 0 }],
          [[0, 5, 7], { id: 7, name: 'gnetum', type: 'vegetable', toggle: 0 }],
          [[0, 5, 8], { id: 8, name: 'h', type: 'vegetable', toggle: 0 }],
          [[0, 9], { id: 9, name: 'iron', type: 'mineral', toggle: 0 }],
          [[0, 9, 10], { id: 10, name: 'jadeite', type: 'mineral', toggle: 0 }],
          [[0, 9, 11], { id: 11, name: 'kryptonite', type: 'mineral', toggle: 0 }],
          [[0, 9, 12], { id: 12, name: 'luddite', type: 'mineral', toggle: 0 }],
          [[0, 9, 12, 13], { id: 13, name: 'malachite', type: 'mineral', toggle: 0 }],
            // intentional gap
          [[20], { id: 20, name: 't', type: 'unknown', toggle: 0 }],
          [[20, 21], { id: 21, name: 'u', type: 'unknown', toggle: 0 }],
          [[20, 22], { id: 22, name: 'v', type: 'unknown', toggle: 0 }],
          [[20, 23], { id: 23, name: 'w', type: 'unknown', toggle: 0 }],
          [[20, 23, 24], { id: 24, name: 'x', type: 'unknown', toggle: 0 }],
          [[20, 23, 25], { id: 25, name: 'y', type: 'unknown', toggle: 0 }],
          [[20, 23, 26], { id: 26, name: 'z', type: 'unknown', toggle: 0 }],
        ];
  
        actual = t.entriesOf();
        expect(actual).toStrictEqual(expected);
      });            
    })

    describe(`when provided a depth`, () => {
      it(`should return all entries`, () => {
        let actual, expected;
        expected = [
          [[0], { id: 0, name: 'root', type: 'vegetable', toggle: 0 }],
          [[0, 1], { id: 1, name: 'alsatian', type: 'animal', toggle: 0 }],
          [[0, 1, 2], { id: 2, name: 'beagle', type: 'animal', toggle: 0 }],
          [[0, 1, 3], { id: 3, name: 'cavador', type: 'animal', toggle: 0 }],
          [[0, 1, 4], { id: 4, name: 'daschund', type: 'animal', toggle: 0 }],
          [[0, 5], { id: 5, name: 'eggplant', type: 'vegetable', toggle: 0 }],
          [[0, 5, 6], { id: 6, name: 'fiddlehead', type: 'vegetable', toggle: 0 }],
          [[0, 5, 7], { id: 7, name: 'gnetum', type: 'vegetable', toggle: 0 }],
          [[0, 5, 8], { id: 8, name: 'h', type: 'vegetable', toggle: 0 }],
          [[0, 9], { id: 9, name: 'iron', type: 'mineral', toggle: 0 }],
          [[0, 9, 10], { id: 10, name: 'jadeite', type: 'mineral', toggle: 0 }],
          [[0, 9, 11], { id: 11, name: 'kryptonite', type: 'mineral', toggle: 0 }],
          [[0, 9, 12], { id: 12, name: 'luddite', type: 'mineral', toggle: 0 }],
          [[0, 9, 12, 13], { id: 13, name: 'malachite', type: 'mineral', toggle: 0 }],
            // intentional gap
          [[20], { id: 20, name: 't', type: 'unknown', toggle: 0 }],
          [[20, 21], { id: 21, name: 'u', type: 'unknown', toggle: 0 }],
          [[20, 22], { id: 22, name: 'v', type: 'unknown', toggle: 0 }],
          [[20, 23], { id: 23, name: 'w', type: 'unknown', toggle: 0 }],
          [[20, 23, 24], { id: 24, name: 'x', type: 'unknown', toggle: 0 }],
          [[20, 23, 25], { id: 25, name: 'y', type: 'unknown', toggle: 0 }],
          [[20, 23, 26], { id: 26, name: 'z', type: 'unknown', toggle: 0 }],
        ];
  
        actual = t.entriesOf(undefined, 0);
        expect(actual).toStrictEqual(expected);
      });
    });
  });
});