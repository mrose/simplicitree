import { describe, expect, it } from "vitest";
import { Tree } from "../tree";

describe(`A Tree's`, () => {

  it(`should have a 'depth', a readonly, unsigned integer representing the greatest length of any key in the Tree`, () => {
    const t = new Tree();

    expect(t.depth).toBeTypeOf('number');
    expect(t.depth).toBe(0);
    expect(() => t.depth(3)).toThrow();

    t.set([0], 'root');
    expect(t.depth).toBe(1);

    t.set([0,1], 'a');
    expect(t.depth).toBe(2);

    t.set([0,2], 'b');
    expect(t.depth).toBe(2);

    t.set([0,2,3], 'c');
    expect(t.depth).toBe(3);
  });

  it(`should have a 'defaultDatum' property, initially set to null`, () => {
    const t = new Tree();
    expect(t.defaultDatum).toBeNull();
  });

  it(`should throw when setting the 'defaultDatum' property to undefined`, () => {
    const t = new Tree();
    expect(() => t.defaultDatum = undefined).toThrow();
  })

  it(`should allow the 'defaultDatum' property to be reset`, () => {
    const t = new Tree();

    t.defaultDatum = {};
    expect(t.defaultDatum).toStrictEqual({});
  });

  it(`should have an 'isEmpty' property as a readonly boolean`, () => {
    const t = new Tree();
    expect(t.isEmpty).toBeTypeOf('boolean');
    expect(t.isEmpty).toBeTruthy();
    expect(() => t.isEmpty = true).toThrow();

    t.set([0], 'root');
    expect(t.isEmpty).toBeFalsy();
  });

  it(`should have a 'roots' property as readonly Keys`, () => {
    const t = new Tree();
    const r0 = t.roots;
    expect(r0).toBeInstanceOf(Array);
    expect(r0.length).toBe(0);

    t.set([0], 'root0');
    let r1 = t.roots;
    expect(r1).toBeInstanceOf(Array);
    expect(r1.length).toBe(1);
    expect(r1).toEqual([[0]]);

    t.set([0, 1], 'first descendent of root0');
    r1 = t.roots;
    expect(r1).toBeInstanceOf(Array);
    expect(r1.length).toBe(1);
    expect(r1).toEqual([[0]]);

    t.set([1], 'root1');
    t.set([2], 'root2');
    const r2 = t.roots;
    expect(r2).toBeInstanceOf(Array);
    expect(r2.length).toBe(3);
    expect(r2).toEqual([[0],[1],[2]]);
  })


  it(`should have a 'size' property as a readonly, unsigned integer`, () => {
    const t = new Tree();
    expect(t.size).toBeTypeOf('number');
    expect(() => t.size(3)).toThrow();

    t.set([0], 'root');
    expect(t.size).toBe(1);

    t.set([0, 1], 'foo');
    expect(t.size).toBe(2);

    t.set([0, 1, 3], 'bar');
    expect(t.size).toBe(3);
  });

});