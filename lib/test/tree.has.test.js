import { describe, expect, it } from "vitest";
import { Tree } from "../tree";

//   has(path: Path): boolean;
describe(`The Tree's has( path ) method`, () => {
  let t;
  beforeEach(() => {
    t = new Tree();
  });

  it(`should return false when provided an invalid path`, () => {
    const a = [];
    const f = function () { return 'foo' };
    const m = new Map();
    const n = null;
    const re = new RegExp("ab+c", "i");
    const s = new Set();
    const u = undefined;

    // path elements
    expect(() => t.has(a)).not.toThrow();
    expect(t.has(a)).toBeFalsy();
    expect(t.has(f)).toBeFalsy();
    expect(t.has(m)).toBeFalsy();
    expect(t.has(n)).toBeFalsy();
    expect(t.has(s)).toBeFalsy();
    expect(t.has(u)).toBeFalsy();

    // full paths
    expect(t.has([a])).toBeFalsy();
    expect(t.has([f])).toBeFalsy();
    expect(t.has([m])).toBeFalsy();
    expect(t.has([n])).toBeFalsy();
    expect(t.has([s])).toBeFalsy();
    expect(t.has([u])).toBeFalsy();

    expect(t.has([0,a])).toBeFalsy();
    expect(t.has([0,f])).toBeFalsy();
    expect(t.has([0,m])).toBeFalsy();
    expect(t.has([0,n])).toBeFalsy();
    expect(t.has([0,s])).toBeFalsy();
    expect(t.has([0,u])).toBeFalsy();

    expect(t.has([0, 9, n])).toBeFalsy();
  });

  it(`should return false when not provided an argument`, () => {
    expect(t.has()).toBeFalsy();
  });

  describe(`when provided a PathElement argument`, () => {
    it(`should return false when the PathElement does not exist in the Tree`, () => {
      const datum = t.defaultDatum;
      t.set([0], datum);
      t.set([0,1], datum);
      t.set([0,1,2], datum);

      expect(t.has(3)).toBeFalsy();
    });

    it(`should return true when the PathElement exists in the Tree`, () => {
      const datum = t.defaultDatum;
      t.set([0], datum);
      t.set([0,1], datum);
      t.set([0,1,2], datum);

      expect(t.has(0)).toBeTruthy();
      expect(t.has(1)).toBeTruthy();
      expect(t.has(2)).toBeTruthy();
    });

  });

  describe(`when provided a FullPath argument`, () => {
    it(`should return false when the FullPath does not exist in the Tree`, () => {
      const datum = t.defaultDatum;
      t.set([0], datum);
      t.set([0,1], datum);
      t.set([0,1,2], datum);

      expect(t.has([0,3])).toBeFalsy();
      expect(t.has([0,1,3])).toBeFalsy();
      expect(t.has([5,7,9])).toBeFalsy();
    });

    it(`should return true when the FullPath exists in the Tree`, () => {
      const datum = t.defaultDatum;
      t.set([0], datum);
      t.set([0,1], datum);
      t.set([0,1,2], datum);

      expect(t.has([0])).toBeTruthy();
      expect(t.has([0,1])).toBeTruthy();
      expect(t.has([0,1,2])).toBeTruthy();
    });

  });

});
