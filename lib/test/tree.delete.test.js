import { describe, expect, it } from "vitest";
import { Tree } from "../tree";

describe(`The Tree's delete( path ) method,`, () => {

  it(`should return false when a path is not provided`, () => {
    const t = new Tree();
    const b = t.delete();
    expect(b).toBeFalsy();
  })

  describe(`When the path is a FullPath,`, () => {
    it(`should remove the specified element from this tree by key and return true`, () => {
      const t = new Tree();
      let b;
      t.set([0], 'root');
      t.set([0,1], 'a');
      t.set([0,2], 'b');
      t.set([0,2,3], 'c');
      t.set([0,4], 'd');
      t.set([0,4,5], 'e');
      expect(t.size).toBe(6);

      b = t.delete([0,2,3]);
      expect(t.size).toBe(5);
      expect(b).toBeTruthy();

      const actual = [...t.keys()];
      const expected = [
        [0],[0,1],[0,2],[0,4],[0,4,5]
      ];
      expect(actual).toStrictEqual(expected);
    });

    it(`should NOT remove children of the specified element from this tree (use prune()... ) and return true`, () => {
      const t = new Tree();
      let b;
      t.set([0], 'root');
      t.set([0,1], 'a');
      t.set([0,2], 'b');
      t.set([0,2,3], 'c');
      t.set([0,4], 'd');
      t.set([0,4,5], 'e');
      expect(t.size).toBe(6);

      b = t.delete([0,4]);
      expect(t.size).toBe(5);
      expect(b).toBeTruthy();

      const actual = [...t.keys()];
      const expected = [
        [0],[0,1],[0,2],[0,2,3],[0,4,5]
      ];
      expect(actual).toStrictEqual(expected);
    });

    it(`should return false when provided a path with at least one invalid element`, () => {
      const t = new Tree();
      t.set([0], 'root');
      t.set([0,1], 'a');
      t.set([0, 2], 'b');

      const a = [];
      const f = function () { return 'foo' };
      const m = new Map();
      const n = null;
      const re = new RegExp("ab+c", "i");
      const s = new Set();
      const u = undefined;

      expect(t.delete([a])).toBeFalsy();
      expect(t.delete([f])).toBeFalsy();
      expect(t.delete([m])).toBeFalsy();
      expect(t.delete([n])).toBeFalsy();
      expect(t.delete([s])).toBeFalsy();
      expect(t.delete([u])).toBeFalsy();

      expect(t.delete([0,a])).toBeFalsy();
      expect(t.delete([0,f])).toBeFalsy();
      expect(t.delete([0,m])).toBeFalsy();
      expect(t.delete([0,n])).toBeFalsy();
      expect(t.delete([0,s])).toBeFalsy();
      expect(t.delete([0,u])).toBeFalsy();

    });

    it(`should return false when the path does not exist`, () => {
      const t = new Tree();
      let b;
      t.set([0], 'root');
      t.set([0,1], 'a');
      t.set([0,2], 'b');
      t.set([0,2,3], 'c');
      t.set([0,4], 'd');
      t.set([0,4,5], 'e');
      expect(t.size).toBe(6);

      b = t.delete([0,9]);
      expect(t.size).toBe(6);
      expect(b).toBeFalsy();
    })

  });

  describe(`when the path is a PathElement,`, () => {

    it(`should remove the specified element from this tree by key and return true`, () => {
      const t = new Tree();
      let b;
      t.set([0], 'root');
      t.set([0, 1], 'a');
      t.set([0, 2], 'b');
      t.set([0, 2, 3], 'c');
      t.set([0, 4], 'd');
      t.set([0, 4, 5], 'e');
      expect(t.size).toBe(6);

      b = t.delete(3);
      expect(t.size).toBe(5);
      expect(b).toBeTruthy();

      const actual = [...t.keys()];
      const expected = [
        [0], [0, 1], [0, 2], [0, 4], [0, 4, 5]
      ];
      expect(actual).toStrictEqual(expected);
    });

    it(`should NOT remove children of the specified element from this tree (use prune()... )`, () => {
      const t = new Tree();
      let b;
      t.set([0], 'root');
      t.set([0, 1], 'a');
      t.set([0, 2], 'b');
      t.set([0, 2, 3], 'c');
      t.set([0, 4], 'd');
      t.set([0, 4, 5], 'e');
      expect(t.size).toBe(6);

      b = t.delete(4);
      expect(t.size).toBe(5);
      expect(b).toBeTruthy();

      const actual = [...t.keys()];
      const expected = [
        [0], [0, 1], [0, 2], [0, 2, 3], [0, 4, 5]
      ];
      expect(actual).toStrictEqual(expected);
    });

    it(`should return false when provided an invalid path`, () => {
      const t = new Tree();
      t.set([0], 'root');
      t.set([0,1], 'a');
      t.set([0, 2], 'b');

      const a = [];
      const f = function () { return 'foo' };
      const m = new Map();
      const n = null;
      const re = new RegExp("ab+c", "i");
      const s = new Set();
      const u = undefined;

      expect(t.delete(a)).toBeFalsy();
      expect(t.delete(f)).toBeFalsy();
      expect(t.delete(m)).toBeFalsy();
      expect(t.delete(n)).toBeFalsy();
      expect(t.delete(s)).toBeFalsy();
      expect(t.delete(u)).toBeFalsy();
    });

    it(`should return false when the path does not exist`, () => {
      const t = new Tree();
      let b;
      t.set([0], 'root');
      t.set([0,1], 'a');
      t.set([0,2], 'b');
      t.set([0,2,3], 'c');
      t.set([0,4], 'd');
      t.set([0,4,5], 'e');
      expect(t.size).toBe(6);

      b = t.delete(9);
      expect(t.size).toBe(6);
      expect(b).toBeFalsy();
    })


  });
});
