import { describe, expect, it } from "vitest";
import { Tree } from "../tree";

// setNode(path: PathElement, datum?: Datum, ancestor?: Path): FullPath;

describe(`A Tree's setNode( pathElement, datum, ancestor ) method`, () => {

  it(`should throw when the path element is undefined`, () => {
    const t = new Tree();
    const path = undefined;
    const datum = t.defaultDatum;
    const ancestor = undefined;
    expect(() => t.setNode(path, datum, ancestor)).toThrow();
  });

  it(`should throw when the path is an array`, () => {
    const t = new Tree();
    const ancestor = 0;
    const datum = t.defaultDatum;
    let path;

    path = [];
    expect(() => t.setNode(path, datum, ancestor)).toThrow();

    path = [1];
    expect(() => t.setNode(path, datum, ancestor)).toThrow();

    path = [1,7];
    expect(() => t.setNode(path, datum, ancestor)).toThrow();
  });

  it(`should throw when the path is not one of: string | number | boolean | object | bigint | symbol`, () => {
    const t = new Tree();
    const str = "foo";
    const num = 38;
    const b = false;
    const obj = { "id": num };
    const bi = BigInt(1000000000);
    const s = Symbol(str);

    const f = function () { return 'foo' };
    const m = new Map();
    const n = null;
    const re = new RegExp("ab+c", "i");
    const set = new Set();
    const u = undefined;

    expect(() => t.setNode()).toThrow();
    expect(() => t.setNode(f)).toThrow();
    expect(() => t.setNode(n)).toThrow();
    expect(() => t.setNode(u)).toThrow();
    expect(() => t.setNode(re)).toThrow();
    expect(() => t.setNode(set)).toThrow();
    expect(() => t.setNode(m)).toThrow();

    /*
    expect(() => t.setNode('')).not.toThrow();
    expect(() => t.setNode('foo')).not.toThrow();
    expect(() => t.setNode(-1)).not.toThrow();
    expect(() => t.setNode(0)).not.toThrow();
    expect(() => t.setNode(1)).not.toThrow();
    expect(() => t.setNode(999)).not.toThrow();
    expect(() => t.setNode(true)).not.toThrow();
    expect(() => t.setNode(false)).not.toThrow();
    expect(() => t.setNode({})).not.toThrow();
    expect(() => t.setNode({ "super": "duper" })).not.toThrow();
    expect(() => t.setNode(s)).not.toThrow();
    expect(() => t.setNode(bi)).not.toThrow();
    */
  });

  it(`should throw when the ancestor is an empty array`, () => {
    const t = new Tree();
    let p, d, a;

    p = 'child';
    d = 'bar';
    a = [];
    expect(() => t.setNode(p, d, a)).toThrow();
  });

  describe(`when no ancestor is provided or ancestor is undefined`, () => {

    it(`should set the root node's key to the path`, () => {
      const t = new Tree();
      const datum = t.defaultDatum;
      let actual, expected, k;
      k = t.setNode('root');
      actual = [...t.entries()];
      expected = [[k, datum]];
      expect(actual).toStrictEqual(expected);
    });

    it(`should overwrite the root node's datum when provided the same key`, () => {
      const t = new Tree();
      let actual, expected;
      const path = 0;
      const datum0 = { id: 0, name: "root" };
      const ancestor = undefined;

      const key0 = t.setNode(path, datum0, ancestor);
      expect(t.size).toBe(1);

      actual = [...t.entries()];
      expected = [
        [[0], datum0],
      ];
      expect(actual).toStrictEqual(expected);

      const datum1 = { id: 0, name: "replacement" };
      const key1 = t.setNode(path, datum1, ancestor);
      expect(t.size).toBe(1);
      actual = [...t.entries()];
      expected = [
        [[0], datum1]
      ];
      expect(actual).toStrictEqual(expected);
    });

    it(`should set the root node's datum to the tree's defaultDatum when no datum is provided, `, () => {
      const t = new Tree();
      const datum = t.defaultDatum;
      let actual, expected, k;
      k = t.setNode(1);
      actual = [...t.entries()];
      expected = [[k, datum]];
      expect(actual).toStrictEqual(expected);
    });

    it(`should set the root node's datum when datum is provided`, () => {
      const t = new Tree();
      const datum = "myDatum";
      let actual, expected, k;
      k = t.setNode(1, datum);
      actual = [...t.entries()];
      expected = [[k, datum]];
      expect(actual).toStrictEqual(expected);
    });
  });

  describe(`when an ancestor is provided`, () => {

    describe(`when the ancestor is an array`, () => {

      it(`should throw when any element in the ancestor array is invalid`, () => {
        const t = new Tree();
        const s = Symbol('foo');
        const bi = BigInt(1000000000);
        const arr = new Array();
        const f = function () { return 'foo' };
        const n = null;
        const u = undefined;
        const re = new RegExp("ab+c", "i");
        const p = 'child';
        const d = 'datum';
        let a;

        a = [f];
        expect(() => t.setNode(p, d, a)).toThrow();

        a = [n];
        expect(() => t.setNode(p, d, a)).toThrow();

        a = [u];
        expect(() => t.setNode(p, d, a)).toThrow();

        a = [re];
        expect(() => t.setNode(p, d, a)).toThrow();

        a = [arr];
        expect(() => t.setNode(p, d, a)).toThrow();

        a = ['root',f];
        expect(() => t.setNode(p, d, a)).toThrow();

        a = ['root',n];
        expect(() => t.setNode(p, d, a)).toThrow();

        a = ['root',u];
        expect(() => t.setNode(p, d, a)).toThrow();

        a = ['root',re];
        expect(() => t.setNode(p, d, a)).toThrow();

        a = ['root',arr];
        expect(() => t.setNode(p, d, a)).toThrow();

        a = ['root',1,f];
        expect(() => t.setNode(p, d, a)).toThrow();

        a = ['root',1,n];
        expect(() => t.setNode(p, d, a)).toThrow();

        a = ['root',1,u];
        expect(() => t.setNode(p, d, a)).toThrow();

        a = ['root',1,re];
        expect(() => t.setNode(p, d, a)).toThrow();

        a = ['root',1,arr];
        expect(() => t.setNode(p, d, a)).toThrow();
      });

      it(`should throw when the path appears in the ancestor array`, () => {
        // 'use set'
        const t = new Tree();
        const path = 7;
        const datum = { "foo": "bar" };
        let ancestor;

        ancestor = [0, 1, 3, 4, 7];
        expect(() => t.setNode(path, datum, ancestor)).toThrow();

        ancestor = [0, 7, 14, 22];
        expect(() => t.setNode(path, datum, ancestor)).toThrow();
      });

      it(`should throw when any element is duplicated within the ancestor array`, () => {
        // 'use set'
        const t = new Tree();
        const path = 7;
        const datum = { "foo": "bar" };
        let ancestor;

        ancestor = [0, 1, 3, 3, 4];
        expect(() => t.setNode(path, datum, ancestor)).toThrow();

        ancestor = [0, 1, 5, 1];
        expect(() => t.setNode(path, datum, ancestor)).toThrow();

        ancestor = ['a', 'b', 'e', 'b'];
        expect(() => t.setNode(path, datum, ancestor)).toThrow();
      });

      it(`should throw when the path already exists with a different ancestor`, () => {
        // i.e. the path exists in a full path that
        // does not match the calculated key (ancestor array + path)
        // 'use set'
        const t = new Tree();
        const path = 7;
        const datum = { id: 7,  name:'albert' };
        let ancestor;
        t.set([0], { id: 0, name: 'root' });
        t.set([0,1], { id: 1, name: 'abigail' });
        ancestor = [0, 1];
        const k = t.setNode(path, datum, ancestor);
        expect(k).toStrictEqual([0, 1, 7]);

        ancestor = [0, 2];
        expect(() => t.setNode(path, datum, ancestor)).toThrow();

        expect(() => t.setNode(1, datum, ancestor)).toThrow();

        ancestor = [0, 1, 7];
        expect(() => t.setNode(1, datum, ancestor)).toThrow();
      });

      // the Tree NO LONGER sets out of order elements when the ancestor is an array
      it(`should throw (and not create intermediate nodes) when the ancestor does not exist`, () => {
        // no orphans, use set()?
        const t = new Tree();
        let actual, ancestor, expected;
        t.set([0], 'root');
        t.set([0,1], 'a');

        expect(() => t.setNode(2, 'b', [0,1,2])).toThrow();
      });

      it(`should set the Tree element by appending the path to the array`, () => {
        const t = new Tree();
        const datum = 'myDatum';
        const ancestor = [0,3,6];
        let actual, expected, key, path;
        t.set([0], datum);
        t.set([0,3], datum);
        t.set([0,3,6], datum);

        path = 9;
        key = t.setNode(path, datum, ancestor);
        expect(key).toEqual([0,3,6,9]);

        expected = datum;
        actual = t.get(key);
        expect(actual).toStrictEqual(expected);

        expected = [
          [[0], datum],
          [[0, 3], datum],
          [[0, 3, 6], datum],
          [[0, 3, 6, 9], datum],
        ];
        actual = [...t.entries()];
        expect(actual).toStrictEqual(expected);
      });

      it(`should set the datum when datum is provided`, () => {
        const t = new Tree();
        const path = 22;
        const datum = { id: 22, name: "Virginia" };
        let actual, ancestor, expected, key;
        t.set([0], null);
        t.set([0,5], null);
        t.set([0,5,12], null);

        ancestor = [0, 5, 12];
        key = t.setNode(path, datum, ancestor);
        actual = [...t.entries()];
        expected = [
          [[0], null],
          [[0,5], null],
          [[0,5,12], null],
          [[0,5,12,22], datum],
        ];
        expect(actual).toStrictEqual(expected);
      });

      it(`should sets datum to the tree's defaultDatum when no datum is provided`, () => {
        const t = new Tree();
        const path = 22;
        const tdv = t.defaultDatum;
        let actual, ancestor, expected, key;
        t.set([0]);
        t.set([0,5]);
        t.set([0,5,12]);

        ancestor = [0, 5, 12];
        key = t.setNode(path, undefined, ancestor);
        actual = [...t.entries()];
        expected = [
          [[0], tdv],
          [[0,5], tdv],
          [[0,5,12], tdv],
          [[0,5,12,22], tdv],
        ];
        expect(actual).toStrictEqual(expected);
      });

    });

    describe(`when the ancestor is a path element`, () => {

      // n.b. empty arrays are invalid and tested above.
      // n.b. ancestor CAN be undefined
      it(`should throw when the ancestor is not one of: string | number | boolean | object | bigint | symbol`, () => {
        const t = new Tree();
        const f = function () { return 'foo' };
        const m = new Map();
        const n = null;
        const re = new RegExp("ab+c", "i");
        const set = new Set();

        const path = 24;
        const datum = t.defaultDatum;

        expect(() => t.setNode(path, datum, f)).toThrow();
        expect(() => t.setNode(path, datum, m)).toThrow();
        expect(() => t.setNode(path, datum, n)).toThrow();
        expect(() => t.setNode(path, datum, re)).toThrow();
        expect(() => t.setNode(path, datum, set)).toThrow();
      });

      it(`should throw when the ancestor does not already exist`, () => {
        const t = new Tree();
        const ancestor = 0;
        const datum = t.defaultDatum;
        const path = 1;
        expect(() => t.setNode(path, datum, ancestor)).toThrow();
      });

      it(`should throw when the path is the same as the ancestor`, () => {
        const t = new Tree();
        t.set([0], { id: 0, name: 'albert' });
        t.set([0,1], { id: 1, name: 'barbara' });
        expect(() => t.setNode(1, {id:1, name: 'bert'}, 1)).toThrow();
      });

      it(`should throw when the path already exists with a different ancestor`, () => {
        const t = new Tree();
        let path;
        const datum = { "id": 7, "name":"G" };
        const ancestor = 4;
        t.set([0], { "id": 0, "name": "A" });
        t.set([0,1], { "id": 1, "name": "B" });
        t.set([0,1,3], { "id": 3, "name": "D" });
        t.set([0,1,3,4], { "id": 4, "name": "E" });

        path = 0;
        expect(() => t.setNode(path, datum, ancestor)).toThrow();

        path = 3;
        expect(() => t.setNode(path, datum, ancestor)).toThrow();
      });

      it(`should throw when the ancestor exists on a different path`, () => {
        // i.e. the path already exists in a full path that
        // does not match the calculated key (derived ancestor array + path)
        const t = new Tree();
        let ancestor, path;
        const datum = { "id": 7, "name":"G" };
        t.set([0], { "id": 0, "name": "A" });
        t.set([0,1], { "id": 1, "name": "B" });
        t.set([0,1,3], { "id": 3, "name": "D" });
        t.set([0,1,3,4], { "id": 4, "name": "E" });
        t.set([0,2], { "id": 2, "name": "C" });
        t.set([0,2,6], { "id": 6, "name": "F" });

        path = 4;
        ancestor = 2;
        expect(() => t.setNode(path, datum, ancestor)).toThrow();

        path = 3;
        ancestor = 0;
        expect(() => t.setNode(path, datum, ancestor)).toThrow();
      });

      it(`should set the Tree element`, () => {
        const t = new Tree();
        let actual, expected;
        const path = 4;
        const datum0 = { id: 0, name: "root" };
        const datum1 = { id: 1, name: "albert" };
        const datum2 = { id: 2, name: "barry" };
        const datum4 = { id: 4, name: "caroline" };
        const ancestor = 2;
        t.set([0], datum0);
        t.set([0,1], datum1);
        t.set([0,1,2], datum2);

        const key = t.setNode(path, datum4, ancestor);
        expect(key).toEqual([0, 1, 2, 4]);
        expect(t.size).toBe(4);

        expected = [
          [[0], datum0],
          [[0, 1], datum1],
          [[0, 1, 2], datum2],
          [[0, 1, 2, 4], datum4],
        ];
        actual = [...t.entries()];
        expect(actual).toStrictEqual(expected);
      });

      it(`should overwrite a previously set datum when provided the same key`, () => {
        const t = new Tree();
        let actual, expected;
        const path = 4;
        const datum0 = { id: 0, name: "root" };
        const datum1 = { id: 1, name: "albert" };
        const datum2 = { id: 2, name: "barry" };
        const datum4 = { id: 4, name: "caroline" };
        const ancestor = 2;
        t.set([0], datum0);
        t.set([0,1], datum1);
        t.set([0,1,2], datum2);

        const key0 = t.setNode(path, datum4, ancestor);
        expect(t.size).toBe(4);

        actual = [...t.entries()];
        expected = [
          [[0], datum0],
          [[0, 1], datum1],
          [[0, 1, 2], datum2],
          [[0, 1, 2, 4], datum4],
        ];
        expect(actual).toStrictEqual(expected);


        const datum5 = { id: 4, name: "delilah" };
        const key1 = t.setNode(path, datum5, ancestor);
        expect(t.size).toBe(4);
        actual = [...t.entries()];
        expected = [
          [[0], datum0],
          [[0, 1], datum1],
          [[0, 1, 2], datum2],
          [[0, 1, 2, 4], datum5],
        ];
        expect(actual).toStrictEqual(expected);
      });

      it(`should set the datum when datum is provided`, () => {
        const t = new Tree();
        let actual, expected;
        const path = 1;
        const datum = { "id": 1, "name": "albert" };
        const ancestor = 2;
        const u = undefined;
        const tdv = t.defaultDatum;

        // root
        const key0 = t.setNode(0, u, u);
        actual = [...t.entries()];
        expected = [
          [[0], tdv]
        ];
        expect(actual).toStrictEqual(expected);

        const key1 = t.setNode(1, datum, 0);
        actual = [...t.entries()];
        expected = [
          [[0], tdv],
          [[0,1], datum],
        ];
        expect(actual).toStrictEqual(expected);
      });

      it(`should sets datum to the tree's defaultDatum when no datum is provided`, () => {
        const t = new Tree();
        let actual, expected;
        const path = 1;
        const ancestor = 2;
        const u = undefined;
        const tdv = t.defaultDatum;

        // root
        const key0 = t.setNode(0, u, u);
        actual = [...t.entries()];
        expected = [
          [[0], tdv]
        ];
        expect(actual).toStrictEqual(expected);

        const key1 = t.setNode(1, u, 0);
        actual = [...t.entries()];
        expected = [
          [[0], tdv],
          [[0,1], tdv],
        ];
        expect(actual).toStrictEqual(expected);
      });
    });
  });
});
