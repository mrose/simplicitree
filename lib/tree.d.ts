// Type definitions for Simplicitree
// Project: Simplicitree
// Definitions by: Mitchell Rose <[Mitch](https://gitlab.com/mrose)>type TK = string | number | symbol | bigint;

import type {
  Accumulator,
  Ancestor,
  Datum,
  Entries,
  Entry,
  FullPath,
  Keys,
  NestedValue,
  NestedValues,
  Order,
  Path,
  PathElement,
  Values,
  K,
  V,
} from "./tree";

export declare class Tree<FullPath, Datum> extends Map<K, V> {
  #private;
  constructor(args: unknown);
  set defaultDatum(datum: Datum);
  get defaultDatum(): Datum;
  get depth(): number;
  get isEmpty(): boolean;
  get roots(): Keys;
  get size(): number;
  ancestorOf: (path: Path) => Datum | undefined;
  cascade: (callbackfn: (datum: Datum, tree: Tree<FullPath, Datum>) => void, path: Path, inclusive?: boolean) => boolean;
  clear: () => void;
  delete: (path: Path) => boolean;
  entries: () => IterableIterator<Entry>;
  entriesOf: (path?: Path, depth?: number) => Entries | undefined;
  everyOf: (callbackfn: (datum: Datum, tree: Tree<FullPath, Datum>) => boolean, path?: Path, depth?: number) => boolean;
  firstDescendentsOf: (path: Path) => Values | undefined;
  forEach: (callbackfn: (datum: Datum, path: FullPath, tree: Map<FullPath, Datum>) => void) => void;
  get: (path: FullPath) => Datum | undefined;
  getNode: (path: Path) => Datum | undefined;
  has: (path: Path) => boolean;
  keys: () => IterableIterator<FullPath>;
  keysOf: (path?: Path, depth?: number) => Keys | undefined;
  prune: (path: Path) => boolean;
  reduceOf: (callbackfn: (accumulator: Accumulator, datum: Datum, tree: Tree<FullPath, Datum>) => Accumulator, initialValue: Accumulator, path?: Path, inclusive?: boolean) => {};
  set: (path: FullPath, datum: Datum) => this;
  setNode: (pathElement: PathElement, datum?: Datum, ancestor?: Ancestor) => FullPath;
  someOf: (callbackfn: (datum: Datum, tree: Tree<FullPath, Datum>) => boolean, path?: Path, depth?: number) => boolean;
  toNestedValuesOf: (path?: Path) => NestedValues;
  traverse: (callbackfn: (datum: Datum, tree: Tree<FullPath, Datum>) => void, path: Path, order?: Order) => boolean;
  values: () => IterableIterator<Datum>;
  valuesOf: (path?: Path, depth?: number) => Values | undefined;
  [Symbol.iterator](): IterableIterator<[FullPath, Datum]>;
  [Symbol.toStringTag]: string;
}
