create examples, esp. w/ mobx

publish benchmarks with large test data set


update methods that receive path, depth, & inclusive should recieve options which contain all
 
new graft()?

update constructor to allow a single collection to be initialised at tree creation time

update constructor to allow multiple " " " 

new mapToTree()? see above

should #keyFilter() return undefined? what would be better

cascade() should not return undefined, see everyOf()

add NaN to invalid path elements

clarify docs to advise if entriesOf() returns in insertion order

entriesOf() should have an inclusive parameter

entriesOf() should not ignore depth when path is not provided

Family group 1: (related iteratee output)

entries, entriesOf, keys, keysOf, values, valuesOf, toNestedValuesOf


Family group 2: (get a callback)

cascade, everyOf, forEach, reduceOf, someOf, traverse


test all methods using depth to use a non-integer number i.e. 1.235
