import first from "lodash/fp/first";
import isArray from "lodash/fp/isArray";
import isBoolean from "lodash/fp/isBoolean";
import isEqual from "lodash/fp/isEqual";
import isFunction from "lodash/fp/isFunction";
import isInteger from "lodash/fp/isInteger";
import isUndefined from "lodash/fp/isUndefined";

export type Accumulator = NonNullable<unknown>;
export type Ancestor = PathElement | FullPath;
export type Datum = NonNullable<V> | null;
export type Entries = Entry[];
export type Entry = [FullPath, Datum];
export type FullPath = PathElement[];
export type Keys = FullPath[];
export type NestedValue =  [Datum, Values];
export type NestedValues = NestedValue[];
export type Order = 'asc' | 'desc';
export type Path = PathElement | FullPath;
export type PathElement = bigint | boolean | number | object | string | symbol;
export type Values = Datum[];
export type K = unknown;
export type V = unknown;

export interface KeyFilterOpts {path?: Path, depth?: number, inclusive?: boolean}

export class Tree extends Map {

  #defaultDatum: Datum;

  constructor(args?: unknown) {
    if (!isUndefined(args)) {
      throw new Error('the Tree constructor does not accept any arguments');
    }

    super();
    this.#defaultDatum = null;
    return this;
  }

  set defaultDatum(datum: Datum) {
    if (isUndefined(datum)) throw new Error('defaultDatum cannot be set to undefined');
    this.#defaultDatum = datum;
  }

  get defaultDatum() {
    return this.#defaultDatum;
  }

  // n.b. making this smarter will be complicated.
  get depth() {
    if (!this.size) return 0;
    let depth = 0;
    const filter = (key:FullPath) => (depth >= key.length);
    const operation = (key:FullPath) => {
      depth = key.length;
      return false;
    };
    this.#keysIterator(filter, operation);
    return depth;
  }

  get isEmpty() {
    return isEqual(this.size, 0);
  }

  get roots() {
    const roots: Keys = [];
    const filter = (key: FullPath) => (key.length !== 1);
    const operation = (key: FullPath) => {
      roots.push(key);
      return false; // true means break
    };
    this.#keysIterator(filter, operation);
    return roots;
  }

  public ancestorOf = (path: Path) => {
    const fullPath = this.#deriveFullPath(path);
    if (!fullPath.length) return undefined;
    if (isEqual(1, fullPath.length)) return undefined; //roots have no ancestor
    const ancestorKey = fullPath.slice(0, fullPath.length - 1);
    return this.get(ancestorKey);
  }

  // Apply a function to a node's descendents, and optionally to the node itself.
  // NOTE: The order of application is NOT guaranteed.
  public cascade = (callbackfn: (datum: Datum, tree: Tree) => void, path: Path, inclusive = false) => {
    if (isUndefined(callbackfn)) throw new Error('undefined is not a function');
    if (!isFunction(callbackfn)) throw new Error('callback is not a function');
    if (!isBoolean(inclusive)) return false;
    if (isUndefined(path)) return false;

    const filter =  this.#keyFilter({path, inclusive});
    if (isUndefined(filter)) return undefined;
    const operation = (datum: Datum) => {
      callbackfn(datum, this);
      return false; // true means break
    };
    this.#entriesIterator(filter, operation);
    return true;
  }

  public clear = () => {
    super.clear();
  }

  public delete = (path: Path) => {
    const fullPath = this.#deriveFullPath(path);
    if (!fullPath.length) return false;
    return super.delete(fullPath);
  }

  public entries = () => {
    return super.entries() as IterableIterator<Entry>;
  }

  public entriesOf = (path?: Path, depth?: number) => {
    if(isUndefined(path)) return [...this.entries()];
    const filter =  this.#keyFilter({path, depth, inclusive:true});
    if (isUndefined(filter)) return undefined;
    const entries: Entries = [];
    const operation = (datum: Datum, path: FullPath) => {
      entries.push([path, datum]);
      return false; // don't break
    };
    this.#entriesIterator(filter, operation);
    return entries;
  }

  public everyOf = (callbackfn: (datum: Datum, tree: Tree) => boolean, path?: Path, depth?: number) => {
    if (isUndefined(callbackfn)) throw new Error('undefined is not a function');
    if (!isFunction(callbackfn)) throw new Error('callback is not a function');
    let every = false;

    const filter =  this.#keyFilter({path, depth, inclusive:true});
    if (isUndefined(filter)) return every;

    // if we got here, at least one entry qualifies for the operation
    every = true;
    const operation = (datum: Datum) => {
      every = callbackfn(datum, this);
      return (every) ? false : true; // true means break
    };
    this.#entriesIterator(filter, operation);
    return every;
  }

  public firstDescendentsOf = (path: Path) => {
    const fullPath = this.#deriveFullPath(path);
    if (!fullPath.length) return undefined;
    const values: Values = [];

    const filter =  this.#keyFilter({path:fullPath, depth:1, inclusive:false});
    if (isUndefined(filter)) return values;

    const operation = (datum: Datum) => {
      values.push(datum);
      return false; // dont' break
    };
    this.#entriesIterator(filter, operation);
    return values;
  }

  public forEach = (callbackfn: (datum: Datum, path: FullPath, tree:Map<FullPath, Datum>) => void): void => {
    if (isUndefined(callbackfn)) throw new Error('undefined is not a function');
    if (!isFunction(callbackfn)) throw new Error('callback is not a function');
    const filter = () => false; // don't continue
    const operation = (datum: Datum, path: FullPath) => {
      callbackfn(datum, path, this);
      return false;
    };
    this.#entriesIterator(filter, operation);
  }

  public get = (path: FullPath) => {
    if (isUndefined(path)) return undefined;
    if (!isArray(path)) {
      throw (new Error('path must be an array, did you mean getNode()?'));
    }

    const fullPath = this.#deriveFullPath(path);
    if (!fullPath.length) return undefined;
    return super.get(fullPath) as Datum;
  }

  public getNode = (path: Path) => {
    if (isArray(path)) {
      return this.get(path);
    }
    const fullPath = this.#deriveFullPath(path);
    if (!fullPath.length) return undefined;
    return this.get(fullPath) as Datum;
  }

  public has = (path: Path) => {
    const fullPath = this.#deriveFullPath(path);
    return isUndefined(fullPath)
      ? false
      : super.has(fullPath);
  }

  public keys = () => {
    return super.keys() as IterableIterator<FullPath>;
  }

  public keysOf = (path?: Path, depth?: number) => {
    if(isUndefined(path)) return [...this.keys()];
    const keys: Keys = [];

    const filter =  this.#keyFilter({path, depth, inclusive:true});
    if (isUndefined(filter)) return undefined;

    const operation = (key: FullPath) => {
      keys.push(key);
      return false; // don't break
    };

    this.#keysIterator(filter, operation);
    return keys;
  }

  public prune = (path: Path) => {
    const fullPath = this.#deriveFullPath(path);
    if (!fullPath.length) return false;
    const done = true;

    const depth = Number.MAX_SAFE_INTEGER - fullPath.length;
    const filter =  this.#keyFilter({path:fullPath, depth, inclusive:true});
    if (isUndefined(filter)) return done;

    const operation = (key: FullPath) => {
      super.delete(key);
      return false; // true means break, so never break
    };
    this.#keysIterator(filter, operation);
    return done;
  }

  public reduceOf = (
    callbackfn: (accumulator: Accumulator, datum: Datum, tree:Tree) => Accumulator,
    initialValue: Accumulator,
    path?: Path,
    inclusive = false
  ) => {
    if (isUndefined(callbackfn)) throw new Error('undefined is not a function');
    if (!isFunction(callbackfn)) throw new Error('callback is not a function');
    if (isUndefined(initialValue)) throw new Error('reduceOf requires an initialValue argument');
    if (!isBoolean(inclusive)) throw new Error('reduceOf requires a boolean inclusive argument');
    let result = initialValue;

    if (isUndefined(path)) inclusive = true;
    const filter =  this.#keyFilter({path, inclusive});
    if (isUndefined(filter)) return result;

    const operation = (datum: Datum) => {
      result = callbackfn(result, datum, this);
      return false; // true means break
    };
    this.#entriesIterator(filter, operation);
    return result;
  };

  public set = (path: FullPath, datum: Datum) => {
    if (!isArray(path)) {
      throw (new Error('path must be an array, did you mean setNode()?'));
    }
    const fullPath = this.#validateFullPath(path);
    datum = (isUndefined(datum)) ? this.#defaultDatum : datum;

    super.set(fullPath, datum);
    return this;
  }

  public setNode = (pathElement: PathElement, datum?: Datum, ancestor?: Ancestor) => {

    let key;
    const pe = this.#validatePathElement(pathElement);
    datum = (isUndefined(datum)) ? this.#defaultDatum : datum;

    // when ancestor is undefined, set a root
    if (isUndefined(ancestor)) {
      key = this.#deriveFullPath(pe);
      if (!key.length) key = [pe];
      super.set(key, datum);
      return key;
    }

    // validate the ancestor
    ancestor = (isArray(ancestor))
      ? this.#validateFullPath(ancestor)
      : this.#validatePathElement(ancestor)
      ;
    const fullPath = this.#deriveFullPath(ancestor);

    if (!fullPath.length) {
      if (!isArray(ancestor)) {
        // throw when ancestor is a pathElement and it doesn't exist
        // this will assure no orphans
        throw new Error('ancestor does not exist');
      }
      // the ancestor does not exist
      throw new Error('ancestor does not exist, did you mean set()?');
      // if we throw to avoid orphans it means that 'proper' insertion
      // order is necessary.
      // we have already validated the array's keys so we could
      // set intermediate nodes if they don't yet exist
      // this.#setIntermediateNodes(fullPath);
      // BUT if an ancestor array is passed they already have access to
      // all the keys anyways, hrrrrm
    }

    if (isEqual(fullPath[fullPath.length - 1], pe)) {
      throw new Error('the path cannot be the ancestor');
    }

    if (fullPath.includes(pe)) {
      throw new Error("path element already exists within the ancestor's tree");
    }

    key = this.#deriveFullPath(pe); // need a reference if it is update
    if (!key.length) {
      key = [...fullPath, pe];
    } else {
      const pk = key.slice(0, key.length - 1);
      if (
        (pk.length !== fullPath.length)
        || (!pk.every((v, i) => (isEqual(v, fullPath[i]))))
      ) {
        const msg = `path element already exists with different ancestor [${key.toString()}], did you mean set()?`;
        throw new Error(msg);
      }
    }
    super.set(key, datum);
    return key;
  }

  public someOf = (callbackfn: (datum: Datum, tree: Tree) => boolean, path?: Path, depth?: number) => {
    if (isUndefined(callbackfn)) throw new Error('undefined is not a function');
    if (!isFunction(callbackfn)) throw new Error('callback is not a function');
    let some = false;

    const filter =  this.#keyFilter({path, depth, inclusive:true});
    if (isUndefined(filter)) return some;

    some = false;
    // if we got here, at least one entry qualifies for operation
    const operation = (datum: Datum) => {
      some = callbackfn(datum, this);
      return some; // true means break
    };
    this.#entriesIterator(filter, operation);
    return some;
  }

  public toNestedValuesOf = (path?: Path) => {
    if (isUndefined(path)) return this.#nestValues(this.roots);
    const fp  = this.#deriveFullPath(path);
    if (!fp.length) return undefined;
    return this.#nestValues([fp]);
  };

  public traverse = (callbackfn: (datum: Datum, tree: Tree) => void, path: Path, order: Order = 'desc') => {
    if (isUndefined(callbackfn)) throw new Error('undefined is not a function');
    if (!isFunction(callbackfn)) throw new Error('callback is not a function');
    if (isUndefined(path)) return false;
    if (!['asc','desc'].includes(order)) return false;
    const fullPath = this.#deriveFullPath(path);
    if (!fullPath.length) return false;
    const keys = this.#generateTraversalKeys(fullPath, order);
    let ret = true;
    keys.forEach((key) => {
      const datum = this.get(key);
      if (isUndefined(datum)) ret = false;
      if (!isUndefined(datum)) callbackfn(datum, this);
    });
    return ret;
  };

  public values = () => {
    return super.values() as IterableIterator<Datum>;
  }

  public valuesOf = (path?: Path, depth?: number) => {
    if(isUndefined(path)) return [...this.values()];
    const values: Values = [];

    const filter =  this.#keyFilter({path, depth, inclusive:true});
    if (isUndefined(filter)) return undefined;

    const operation = (datum: Datum) => {
      values.push(datum);
      return false; // don't break
    };
    this.#entriesIterator(filter, operation);
    return values;
  }

  // helper methods & etc -----------------------------------------------------

  // while a reverse tip lookup would likely be more performant
  // than iteration it would be complicated (see depth property too)
  // n.b. cant return the path directly since it's held by value
  #deriveFullPath = (path: Path): FullPath => {
    if (isUndefined(path)) return [];
    const matched: Keys = [];
    let p: FullPath = [];
    try {
      p = (isArray(path))
        ? this.#validateFullPath(path)
        : [ this.#validatePathElement(path) ]
        ;
    }
    catch (e) {
      return [];
    }

    // we can avoid iteration if we have it
    if (super.has(p)) return p;

    // n.b. prolly faster to loop tips first
    p = p.reverse();
    const filter = (key: FullPath) => {
      const rk:FullPath = [...key].reverse();
      const found = isArray(path)
        // all of rk must match when provided a fullPath
        ? rk.every((el, i) => (el === p[i]))
        : isEqual(first(rk), first(p))
        ;
        return !found;
    };

    const operation = (key:FullPath) => {
      matched.push(key);
      return true; // break;
    };
    this.#keysIterator(filter, operation);
    return (matched.length) ? matched[0] : [];
  }

  #entriesIterator = (
    filter: (key: FullPath) => boolean,
    operation: (datum: Datum, path: FullPath, tree: Tree) => boolean,
  ) => {

    const iterator = this.entries();
    for (const entry of iterator) {
      if (filter(entry[0])) continue;
      if (operation(entry[1], entry[0], this)) break;
    }
  };

  #generateTraversalKeys = (fullPath: FullPath, order: Order): Keys => {
    const keys: Keys = [];
    let last = [];
    fullPath.forEach((value) => {
      last = (keys.length) ? [...keys[keys.length - 1]] : [];
      last.push(value);
      keys.push(last);
    });
    // the natural order is descending, that is, from
    // a key with a length of one, then two, etc.
    return (order === 'desc') ? keys : keys.reverse();
  }

  #keyFilter = (opts: KeyFilterOpts = {}) => {
    let minDepth = 1;
    let maxDepth = Number.MAX_SAFE_INTEGER;
    let fullPath: FullPath = [];
    const {
      path,
      depth,
      inclusive = false
    } = opts;

    if (!isUndefined(depth)) {
      if (!isInteger(depth) || (depth < 0)) return undefined;
      if (depth >  Number.MAX_SAFE_INTEGER) return undefined;
      // when path is undefined it's roots plus
      maxDepth = 1 + depth;     
    }
    
    if (!isUndefined(path)) {
      fullPath = this.#deriveFullPath(path);
      if (!fullPath.length) return undefined;
      if (!isUndefined(depth)) {
        if ((Number.MAX_SAFE_INTEGER - depth) < fullPath.length) return undefined;
        maxDepth = fullPath.length + depth;
        if (maxDepth < 0) return undefined;
      }
      minDepth = inclusive ? fullPath.length : fullPath.length + 1;
    }

    return (key: FullPath) => {
      const filteredOut = true;
      if (key.length < minDepth) return filteredOut;  // somebodys ancestors
      if (maxDepth < key.length) return filteredOut;
      if (!fullPath.every((k, i) => ((k === key[i])))) return filteredOut;
      return false;
    }
  }


  #keysIterator = (
    filter: (key: FullPath) => boolean,
    operation: (key: FullPath, tree:Tree) => boolean,
  ) => {

    const iterator = this.keys();
    for (const key of iterator) {
      if (filter(key)) continue;
      if (operation(key, this)) break;
    }
  };

  #nestValues = (keys: Keys) => {
    const nestedValues: NestedValues = [];
    // TODO: if keys are sortable sort them? flatten first to check ;)
    keys.forEach((fullPath) => {
      const datum = this.get(fullPath);
      if (isUndefined(datum)) return;
      const descendentKeys: Keys = [];

      const path = fullPath;
      const depth = 1;
      const inclusive = false;
      const filter =  this.#keyFilter({path, depth, inclusive});
  
      const operation = (fullPath:FullPath) => {
        descendentKeys.push(fullPath);
        return false; // true means break
      };
      if (!isUndefined(filter)) this.#keysIterator(filter, operation);

      const descendentValues: Values = descendentKeys.length
        ? this.#nestValues(descendentKeys)
        : []
        ;
      const nv: NestedValue = [datum, descendentValues];
      nestedValues.push(nv);
    });

    return nestedValues;
  };


  /*
  #setIntermediateNodes(fullPath: FullPath) {
    const partialPath:FullPath = [];
    fullPath.forEach((fpe:PathElement) => {
      partialPath.push(fpe);
      // super wont work since arrays are by reference
      if (!this.has(partialPath)) {
        super.set(partialPath, this.defaultDatum);
      }
    });

  }
  */

  #validateFullPath = (path: unknown[]): FullPath => {
    if (!isArray(path)) {
      throw new Error('path must be an array');
    }
    if (!path.length) {
      throw new Error('path cannot be an empty array');
    }

    const p = path.map((v: unknown, idx: number) => {
      return this.#validatePathElement(v, idx);
    });

    return p;
  }

  #validatePathElement = (pathElement: unknown, idx: number | undefined = undefined): PathElement => {

    if (isArray(pathElement)) {
      throw new Error(this.#errMsg('an array', idx));
    }

    // for comprehensibility
    if (pathElement === null) {
      throw new Error(this.#errMsg('null', idx));
    }

    if (isUndefined(pathElement)) {
      throw new Error(this.#errMsg('undefined', idx));
    }

    if (pathElement instanceof RegExp) {
      throw new Error(this.#errMsg('a regular expression', idx));
    }

    if (pathElement instanceof Set) {
      throw new Error(this.#errMsg('a Set', idx));
    }

    if (pathElement instanceof Map) {
      throw new Error(this.#errMsg('a Map', idx));
    }

    const t = typeof pathElement;
    if (!(['string', 'number', 'boolean', 'object', 'bigint', 'symbol'].includes(t))) {
      throw new Error(this.#errMsg(t, idx));
    }

    return pathElement;
  }

  #errMsg = (txt: string, idx: number | undefined) => {
    return `path element cannot be ${txt}${(isInteger(idx)) ? ` at element ${idx}` : ""
      }`;
  }
}
