import React from 'react';
import ReactDOM from "react-dom/client";
import { App } from "./exampleAntd/App";
import './css/main.css';

const container = document.getElementById('root');

if (container !== null) {
  const root = ReactDOM.createRoot(container);

  root.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>,
  )
  
}
if (container == null) {
  console.log(`no root element exists`);
}
