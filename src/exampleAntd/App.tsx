import React, { useState } from 'react';
import './App.css';
import { Tree } from 'antd';
import type { TreeDataNode, TreeProps } from 'antd';
import { prepareAntd } from '../util/tree/utils';

type TreeData = TreeDataNode[];

// addtl possible attributes: disabled: boolean, disableCheckbox: boolean,
const td: TreeData = prepareAntd();


const AntdTree: React.FC<TreeData> = () => {
  const initialKeys: string[] = [];
  const initialChecked: string[] = [];
  const [expandedKeys, setExpandedKeys] = useState<React.Key[]>(initialKeys);
  const [checkedKeys, setCheckedKeys] = useState<React.Key[]>(initialChecked);
  const [selectedKeys, setSelectedKeys] = useState<React.Key[]>([]);
  const [autoExpandParent, setAutoExpandParent] = useState<boolean>(true);

  const onExpand: TreeProps['onExpand'] = (expandedKeysValue) => {
    // console.log('onExpand', expandedKeysValue);
    // if not set autoExpandParent to false, if children expanded, parent can not collapse.
    // or, you can remove all expanded children keys.
    setExpandedKeys(expandedKeysValue);
    setAutoExpandParent(false);
  };

  const onCheck: TreeProps['onCheck'] = (checkedKeysValue) => {
    // console.log('onCheck', checkedKeysValue);
    setCheckedKeys(checkedKeysValue as React.Key[]);
  };

  const onSelect: TreeProps['onSelect'] = (selectedKeysValue, info) => {
    // console.log('onSelect', info);
    setSelectedKeys(selectedKeysValue);
  };

  return (
    <Tree
      autoExpandParent={autoExpandParent}
      blockNode
      checkable
      checkedKeys={checkedKeys}
      expandedKeys={expandedKeys}
      onCheck={onCheck}
      onExpand={onExpand}
      onSelect={onSelect}
      selectedKeys={selectedKeys}
      treeData={td}
    />
  );
};


const App: React.FC = () => {
  return (
    <>
      <h1>Examples</h1>
      <h3>Antd UI TreeView</h3>
      <div className="card">
        <AntdTree {...td} />
      </div>
    </>
  )
}

export { App }
