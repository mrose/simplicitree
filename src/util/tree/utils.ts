import { Tree } from 'simplicitree';
import type { Accumulator, Datum } from 'simplicitree';
import { isUndefined, map } from 'lodash/fp';
import { IntRange } from 'type-fest';
import type { TreeDataNode } from 'antd';


export declare type AmvAccumulator = Accumulator & {
 count: number,
 checkedTotal: number,
};

declare type Checked = 0 | 1;
declare type Id = IntRange<0, 50>;

export declare type Amv = Datum &  {
  id: Id,
  label: string,
  checked: Checked;
  checkedChildren?: number,
  totalChildren?: number,
  path?: Id[];
};

// a 'distinct' tree is one where every id is unique
const prepareDistinctTree = () => {
  const t = new Tree();
  t.set([0],          { id: 0, label: 'animals', checked: 0, path: [0] });

  t.set([0, 1],       { id: 1, label: 'dogs',  checked: 0, path: [0, 1] });
  t.set([0, 1, 2],    { id: 2, label: 'alsatian',  checked: 0, path: [0, 1, 2] });
  t.set([0, 1, 3],    { id: 3, label: 'beagle',  checked: 0, path: [0, 1, 3] });
  t.set([0, 1, 4],    { id: 4, label: 'cavador',  checked: 0, path: [0, 1, 4] });
  t.set([0, 1, 5],    { id: 5, label: 'daschund',  checked: 0, path: [0, 1, 5] });

  t.set([0, 6],       { id: 6, label: 'cats',  checked: 0, path: [0, 6] });
  t.set([0, 6, 7],    { id: 7, label: 'abyssinian',  checked: 0, path: [0, 6, 7] });
  t.set([0, 6, 8],    { id: 8, label: 'bengal',  checked: 0, path: [0, 6, 8] });
  t.set([0, 6, 9],    { id: 9, label: 'chartreux',  checked: 0, path: [0, 6, 9] });
  t.set([0, 6, 10],   { id: 10, label: 'donskoy',  checked: 0, path: [0, 6, 10] });

  t.set([0, 11],      { id: 11, label: 'monkeys',  checked: 0, path: [0, 11] });
  t.set([0, 11, 12],  { id: 12, label: 'angwantibo',  checked: 0, path: [0, 11, 12] });
  t.set([0, 11, 13],  { id: 13, label: 'baboon',  checked: 0, path: [0, 11, 13] });
  t.set([0, 11, 14],  { id: 14, label: 'capuchin',  checked: 0, path: [0, 11, 14] });
  t.set([0, 11, 15],  { id: 15, label: 'dwarf lemur',  checked: 0, path: [0, 11, 15] });

  t.set([16],           { id: 16, label: 'vegetables', checked: 0, path: [16] });

  t.set([16, 17],       { id: 17, label: 'fruits', checked: 0, path: [16, 17] });
  t.set([16, 17, 18],   { id: 18, label: 'apple', checked: 0, path: [16, 17, 18] });
  t.set([16, 17, 19],   { id: 19, label: 'banana', checked: 0, path: [16, 17, 19] });
  t.set([16, 17, 20],   { id: 20, label: 'cherry', checked: 0, path: [16, 17, 20] });

  t.set([16, 21],       { id: 21, label: 'roots', checked: 0, path: [16, 21] });
  t.set([16, 21, 22],   { id: 22, label: 'carrot', checked: 0, path: [16, 21, 22] });
  t.set([16, 21, 23],   { id: 23, label: 'potatoe', checked: 0, path: [16, 21, 23] });
  t.set([16, 21, 24],   { id: 24, label: 'turnip', checked: 0, path: [16, 21, 24] });


  t.set([25],           { id: 25, label: 'minerals', checked: 0, path: [25] });
  t.set([25, 26],       { id: 26, label: 'iron', checked: 0, path: [25, 26] });
  t.set([25, 27],       { id: 27, label: 'jadeite', checked: 0, path: [25, 27] });
  t.set([25, 28],       { id: 28, label: 'kryptonite', checked: 0, path: [25, 28] });
  t.set([25, 29],       { id: 29, label: 'luddite', checked: 0, path: [25, 29] });
  t.set([25, 30],       { id: 30, label: 'malachite', checked: 0, path: [25, 30] });
  return t;
};

// interestingly, the recursion only works when stated this way & not in reverse...
type AmvNestedValues = AmvEntry[];
type AmvEntry = [Amv, AmvNestedValues];

const antdXlate = (nvs: AmvNestedValues): TreeDataNode[] => {
  return map((nv: AmvEntry) => {
    const [ {id, label, ...others}, values] = nv;
    return {
      key: id.toString(),
      title: label,
      ...others,
      children: values.length > 0 ? antdXlate(values) : [],
    };
  })(nvs);
}

const prepareAntd = () => {
  const t:Tree = prepareDistinctTree();
  const n = t.toNestedValuesOf() as AmvNestedValues;
  if (isUndefined(n)) return [];
  //console.log('antd tree:', n)
  return antdXlate(n);
};

export {
  prepareDistinctTree,
  prepareAntd,
};