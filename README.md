# simplicitree

![Thee Simplicit Tree](/img/simplicitree-264x264.png)

This Tree object is a tree of nodes where each node is a key-value pair and the original insertion order is remembered.
Keys can be bigints, booleans, numbers, objects, strings, or symbols, or arrays of those primitives.
Values cannot be undefined.

## Motivation
A common practice when using relational databases is to keep parent-child
relationships in a join table or as self referential 'parent' keys in the data table itself. Complex code or queries must be created to recurse through those relationships. This activity can become highly repetitive over time because column names and data varies from table to table, and apis based on that data may be inconsistent.

The Tree provides well known collection methods for recursive iteration as well as standard methods for data insertion, retrieval, and deletion by formally seperating node relationships from the data itself. It can be used as a standalone utility to efficiently generate nested tuples. Access times that are sublinear on the number of elements in the collection can be expected, e.g. better than O(N).

Typescript typings are provided.

Terminology:

- Nodes are key-value tuples (also known as Entries).
- Ancestors and Descendents are Nodes.
- Root Nodes have no Ancestor.
- A key is called a "Path" whether primitive or complex.
- The value related to a key is called a "Datum" (singular of Data).

Trees with single or multiple Root nodes are supported.
Keys must be unique; duplicate primitives can be made unique using an array representing the path to the node.
As nodes are added to the Tree, references to datum objects are preserved and returned.

## Installation

```sh
$ npm install simplicitree
```

## Import or Require

```bash
import { Tree } from "simplicitree";

```

## API

```
// Create a tree
const t = new Tree();

properties:
.defaultDatum
.depth
.hasDescendents
.isEmpty
.roots
.size

methods:
.ancestorOf(path)
.cascade(callbackfn, path, inclusive?)
.clear()
.delete(path)
.entries()
.entriesOf(path?, depth?)
.everyOf(callbackfn, path?, depth?)
.firstDescendentsOf(path)
.forEach(callbackfn)
.get(path)
.getNode(path)
.has(path)
.keys()
.keysOf(path?, depth?)
.prune(path)
.reduceOf(callbackfn, initialValue, path?, inclusive?)
.set(path, datum?)
.setNode(path, datum?, ancestor?)
.someOf(callbackfn, path?, depth?)
.toNestedValuesOf(path?)
.traverse(callbackfn, path, order?)
.values()
.valuesOf(path?, depth?)


method arguments definitions:

path: a primitive, or a unique array of primitives, which identify a single node

inclusive: a boolean representing how callbacks with path arguments are processed
 - when true, the node related to the path argument is provided to the callback function
 - when false, the node is not provided to the callback function
 - ignored when a method's path argument is optional and the path argument is not provided

depth: a non-negative integer describing how descendents are provided to callback functions
 - methods which offer a depth argument are always inclusive
 - when a path argument is optional and the path argument is not provided, a depth of zero refers to Root nodes
 - when a path argument is provided, a depth of zero refers to the node related to the path

```

## More

<https://gitlab.com/mrose/simplicitree/-/wikis/Tree>

